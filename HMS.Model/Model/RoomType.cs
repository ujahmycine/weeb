﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class RoomType
    {
        public int RoomTypeId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool? Active { get; set; }
        [Required]
        public string Max_Discount { get; set; }
        [Required]
        public string DEscription { get; set; }
        [Required]
        public decimal? RoomCharge { get; set; }
        public int NumOfNight { get; set; }
    }
}
