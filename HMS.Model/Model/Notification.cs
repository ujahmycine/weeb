﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime Datetime { get; set; }
        public bool? Status { get; set; }
        public Person Person { get; set; }
    }
}
