﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class RoomBookingLog
    {
        public int Id { get; set; }
        public RoomBookings RoomBookings { get; set; }
        public Payment Payment { get; set; }
        public DateTime BookingDate { get; set; }
        public int NumOfNight { get; set; }
        public int NumOfRooms { get; set; }
        public RoomStatus CheckInStatus { get; set; }

    }
}
