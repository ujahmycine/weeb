﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
   public class CheckInStatus
    {
       public int Id { get; set; }
       public string Name { get; set; }
    }
}
