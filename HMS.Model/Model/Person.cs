﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
   public class Person
    {
        private string fullName;
        public int PersonId { get; set; }
       [Required]
       public string Surname { get; set; }
       [Required]
       public string FirstName { get; set; }
       public string OtherName { get; set; }
       public string Email { get; set; }
       [Required]
       public string MobilePhone { get; set; }
       [Required]
       public string Address { get; set; }
       public bool? IsCorperate { get; set; }
       public string IdNumber { get; set; }
      
       public IdType IdType { get; set; }
       public Gender Gender { get; set; }
       public Country Country { get; set; }

        public string FullName
        {
            get { return Surname + " " + FirstName+ " "+ OtherName; }
            set { fullName = value; }
        }
    }
}
