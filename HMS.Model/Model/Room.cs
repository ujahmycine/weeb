﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class Room
    {
        public int RoomId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool? Active { get; set; }
        [Required]
        [Display(Name= "Room Name")]
        public RoomType RoomType { get; set; }
        [Required]
        public string Description { get; set; }
        [Display(Name = "Maximum Occupant")]
        public int Max_Occupant { get; set; }
        [Required]
        [Display(Name = "Maximum Number Of Adults")]
        public int MaxAdult { get; set; }
        [Required]
        [Display(Name = "Room State")]
        public RoomStatus RoomStatus { get; set; }
        public int? NoOfChildren { get; set; }
        public DateTime ArivalDate { get; set; }
    }
}
