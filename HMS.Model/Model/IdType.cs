﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class IdType
    {
        public int IdTypeId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
       
    }
}
