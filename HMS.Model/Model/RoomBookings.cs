﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class RoomBookings
    {
        public int RoomBookingId { get; set; }
        [Required]
        public DateTime CheckInDate { get; set; }
        [Required]
        public DateTime CheckOutDate { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }
        [Required]
        public DateTime DepatureTime { get; set; }
        [Required]
        public string Notes { get; set; }
        [Required]
        public bool? Paid { get; set; }
        public Room Room { get; set; }
       public RoomType RoomType { get; set; }
        [Required]
        public Person Person { get; set; }
       
        public bool? CheckedIn { get; set; }
        public bool? CheckedOut { get; set; }
        public DateTime BookingDate { get; set; }
        public int? NumOfNight { get; set; }
        public int? NumOfRooms { get; set; }
        public Payment Payment { get; set; }
        public RoomStatus CheckInStatus { get; set; }
      
    }
}
