﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class RoomBookingPayment
    {
        public int RoomBookingPaymentId { get; set; }
        [Required]
        public RoomBookings RoomBooking { get; set; }
        [Required]
        public Payment Payment { get; set; }

    }
}
