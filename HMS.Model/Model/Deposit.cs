﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
    public class Deposit
    {
        public int DepositId { get; set; }
        public string DEscription { get; set; }
        public RoomBookings RoomBooking { get; set; }
    }
}
