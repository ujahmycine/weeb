﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
   public class User
    {

    
        public int UserId { get; set; }
        [Required]
       public string UserName { get; set; }
       [Required]
       [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
       [DataType(DataType.Password)]
       public string PasswordHash { get; set; }
       [Required]
       public bool? Active { get; set; }
       public DateTime? LastLogin { get; set; }
       public string IpAddress { get; set; }
       public Person Person { get; set; }
       public AccountType AccountType { get; set; }

       
    }
}
