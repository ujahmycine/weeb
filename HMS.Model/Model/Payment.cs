﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Model
{
   public class Payment
    {
       public int PaymentId { get; set; }
       public decimal? Amount { get; set; }
       public string Description { get; set; }
       public string PaymentGateWay { get; set; }
       public DateTime DatePaid { get; set; }
       public PaymentType PaymentType { get; set; }
       public Person Person { get; set; }
       public Service Service { get; set; }
        public string Pin { get; set; }
        public string barcodeImageUrl { get; set; }
        public RoomBookings RoomBookings { get; set; }
    }
}
