﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class RoomStatusTranslator : TranslatorBase<RoomStatus, ROOM_STATUS>
    {
        public override RoomStatus TranslateToModel(ROOM_STATUS roomStatusEntity)
        {
            try
            {
                RoomStatus roomStatus = null;
                if (roomStatusEntity != null)
                {
                    roomStatus = new RoomStatus();
                    roomStatus.RoomStatusId = roomStatusEntity.Room_Status_Id;
                    roomStatus.Name = roomStatusEntity.Name;
                    roomStatus.Active = roomStatusEntity.Active;
                    
                }
                return roomStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ROOM_STATUS TranslateToEntity(RoomStatus roomStatus)
        {
            try
            {
                ROOM_STATUS roomStatusEntity = null;
                if (roomStatus != null)
                {
                    roomStatusEntity = new ROOM_STATUS();
                    roomStatusEntity.Room_Status_Id = roomStatus.RoomStatusId;
                    roomStatusEntity.Name = roomStatus.Name;
                    roomStatusEntity.Active = roomStatus.Active;
                }
                return roomStatusEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
