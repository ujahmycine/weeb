﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class ServiceTranslator : TranslatorBase<Service, SERVICE>
    {
        public override Service TranslateToModel(SERVICE serviceEntity)
        {
            try
            {
                Service service = null;
                if (serviceEntity != null)
                {
                    service = new Service();
                    service.ServiceId = serviceEntity.Service_Id;
                    service.Name = serviceEntity.Name;
                    service.Active = serviceEntity.Active;
                }
                return service;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SERVICE TranslateToEntity(Service service)
        {
            try
            {
                SERVICE serviceEntity = null;
                if (service != null)
                {
                    serviceEntity = new SERVICE();
                    serviceEntity.Service_Id = service.ServiceId;
                    serviceEntity.Name = service.Name;
                    serviceEntity.Active = service.Active;

                }
                return serviceEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
