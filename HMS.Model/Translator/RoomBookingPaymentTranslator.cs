﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class RoomBookingPaymentTranslator : TranslatorBase<RoomBookingPayment, ROOM_BOOKING_PAYMENT>
    {
        private PaymentTranslator paymentTranslator;
        private RoomBookingTranslator roomBookingTranslator;
        public RoomBookingPaymentTranslator() 
        {
            paymentTranslator = new PaymentTranslator();
            roomBookingTranslator = new RoomBookingTranslator();
        }
        public override RoomBookingPayment TranslateToModel(ROOM_BOOKING_PAYMENT roomBookingPaymentEntity)
        {
            try
            {
                RoomBookingPayment roomBookingPayment = null;
                if (roomBookingPaymentEntity != null)
                {
                    roomBookingPayment = new RoomBookingPayment();
                    roomBookingPayment.RoomBookingPaymentId = roomBookingPaymentEntity.Room_Booking_Payment_Id;
                    roomBookingPayment.RoomBooking = roomBookingTranslator.TranslateToModel(roomBookingPaymentEntity.ROOM_BOOKING);
                    roomBookingPayment.Payment = paymentTranslator.TranslateToModel(roomBookingPaymentEntity.PAYMENT);
                }
                return roomBookingPayment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ROOM_BOOKING_PAYMENT TranslateToEntity(RoomBookingPayment roomBookingPayment)
        {
            try
            {
                ROOM_BOOKING_PAYMENT roomBookingPaymentEntity = null;
                if (roomBookingPayment != null)
                {
                    roomBookingPaymentEntity = new ROOM_BOOKING_PAYMENT();
                    roomBookingPaymentEntity.Room_Booking_Payment_Id = roomBookingPayment.RoomBookingPaymentId;
                    roomBookingPaymentEntity.Room_Booking_Id = roomBookingPayment.RoomBooking.RoomBookingId;
                    roomBookingPaymentEntity.Payment_Id = roomBookingPayment.Payment.PaymentId;

                }
                return roomBookingPaymentEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
