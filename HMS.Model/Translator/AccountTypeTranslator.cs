﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class AccountTypeTranslator : TranslatorBase<AccountType, ACCOUNT_TYPE>
    {
        public override AccountType TranslateToModel(ACCOUNT_TYPE accountTypeEntity)
        {
            try
            {
                AccountType accountType = null;
                if (accountTypeEntity != null)
                {
                    accountType = new AccountType();
                    accountType.AccountTypeId = accountTypeEntity.Account_Type_Id;
                    accountType.Name = accountTypeEntity.Name;
                    accountType.Active = accountTypeEntity.Active;
                }
                return accountType;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ACCOUNT_TYPE TranslateToEntity(AccountType accountType)
        {
            try
            {
                ACCOUNT_TYPE accountTypeEntity = null;
                if (accountType != null)
                {
                    accountTypeEntity = new ACCOUNT_TYPE();
                    accountTypeEntity.Account_Type_Id = accountType.AccountTypeId;
                    accountTypeEntity.Name = accountType.Name;
                    accountTypeEntity.Active = accountType.Active;

                }
                return accountTypeEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
