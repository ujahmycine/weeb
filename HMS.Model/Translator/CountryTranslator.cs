﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class CountryTranslator:TranslatorBase<Country, COUNTRY>
    {
        public override Country TranslateToModel(COUNTRY countryEntity)
        {
            try
            {
                Country country = null;
                if (countryEntity != null)
                {
                    country = new Country();
                    country.CountryId = countryEntity.Country_Id;
                    country.Name = countryEntity.Name;
                    country.Active = countryEntity.Active;
                }
                return country;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override COUNTRY TranslateToEntity(Country country)
        {
            try
            {
                COUNTRY countryEntity = null;
                if (country != null)
                {
                    countryEntity = new COUNTRY();
                    countryEntity.Country_Id = country.CountryId;
                    countryEntity.Name = country.Name;
                    countryEntity.Active = country.Active;
                    
                }
                return countryEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
