﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
   public class UserTranslator:TranslatorBase<User,USER>
    {
       private PersonTranslator personTranslator;
       private AccountTypeTranslator accountTypeTranslator;
       public UserTranslator() 
       {
           personTranslator = new PersonTranslator();
           accountTypeTranslator = new AccountTypeTranslator();
       }
       public override User TranslateToModel(USER userEntity) 
       {
           try 
           {
               User user = null;
               if (userEntity != null) 
               {
                   user = new User();
                   user.UserId = userEntity.User_Id;
                   user.UserName = userEntity.Username;
                   user.PasswordHash = userEntity.Password_Hash;
                   user.LastLogin = userEntity.Last_Login;
                   user.IpAddress = userEntity.Ip_Address;
                   user.Active = userEntity.Active;
                   user.AccountType = accountTypeTranslator.TranslateToModel(userEntity.ACCOUNT_TYPE);
                   user.Person = personTranslator.TranslateToModel(userEntity.PERSON);
               }
               return user;
           }
           catch (Exception) 
           {
               throw;
           }
       }

       public override USER TranslateToEntity(User user) 
       {
           try 
           {
               USER userEntity = null;
               if (user != null) 
               {
                   userEntity = new USER();
                   userEntity.User_Id = user.UserId;
                   userEntity.Username = user.UserName;
                   userEntity.Active = user.Active;
                   userEntity.Last_Login = user.LastLogin;
                   userEntity.Ip_Address = user.IpAddress;
                   userEntity.Person_Id = user.Person.PersonId;
                   userEntity.Account_Type_Id = user.AccountType.AccountTypeId;
                    userEntity.Password_Hash = user.PasswordHash;
               }
               return userEntity;
           }
           catch (Exception) 
           {
               throw;
           }
       }
    }
}
