﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class GenderTranslator : TranslatorBase<Gender, GENDER>
    {
        public override Gender TranslateToModel(GENDER genderEntity)
        {
            try
            {
                Gender gender = null;
                if (genderEntity != null)
                {
                    gender = new Gender();
                    gender.GenderId = genderEntity.Gender_Id;
                    gender.Name = genderEntity.Name;
                    gender.Active = genderEntity.Active;
                }
                return gender;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override GENDER TranslateToEntity(Gender gender)
        {
            try
            {
                GENDER genderEntity = null;
                if (gender != null)
                {
                    genderEntity = new GENDER();
                    genderEntity.Gender_Id = gender.GenderId;
                    genderEntity.Name = gender.Name;
                    genderEntity.Active = gender.Active;

                }
                return genderEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
