﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class PersonTranslator:TranslatorBase<Person,PERSON>
    {
        private GenderTranslator genderTranslator;
        private IdTypeTranslator idTypeTranslator;
        private CountryTranslator countryTranslator;
        public PersonTranslator()
        {
            genderTranslator = new GenderTranslator();
            idTypeTranslator = new IdTypeTranslator();
            countryTranslator = new CountryTranslator();
        }
        public override Person TranslateToModel(PERSON personEntity) 
        {
            try 
            {
                Person person = null;
                if (personEntity != null) 
                {
                    person = new Person();
                    person.PersonId = personEntity.Person_Id ;
                    person.Surname = personEntity.Surname;
                    person.FirstName = personEntity.FirstName;
                    person.OtherName = personEntity.OtherName;
                    person.Address = personEntity.Address;
                    person.Email = personEntity.Email;
                    person.IsCorperate = personEntity.Is_Corperate_Or_Group;
                    person.MobilePhone = personEntity.Phone_Number;
                    person.Country = countryTranslator.TranslateToModel(personEntity.COUNTRY);
                    person.IdType = idTypeTranslator.TranslateToModel(personEntity.ID_TYPE);
                    person.Gender = genderTranslator.TranslateToModel(personEntity.GENDER);
                    person.IdNumber = personEntity.Id_Number;
                }
                return person;
            }
            catch (Exception) 
            {
                throw;
            }
        }
        public override PERSON TranslateToEntity(Person person) 
        {
            try 
            {
                PERSON personEntity = null;
                if (person != null) 
                {
                    personEntity = new PERSON();
                    personEntity.Person_Id = person.PersonId;
                    personEntity.Surname = person.Surname;
                    personEntity.FirstName = person.FirstName;
                    personEntity.OtherName = person.OtherName;
                    personEntity.Address = person.Address;
                    personEntity.Email = person.Email;
                    personEntity.Is_Corperate_Or_Group = person.IsCorperate;
                    personEntity.Country_Id = person.Country.CountryId;
                    personEntity.Gender_Id = person.Gender.GenderId;
                    personEntity.Id_Type_Id = person.IdType.IdTypeId;
                    personEntity.Id_Number = person.IdNumber;
                    personEntity.Phone_Number = person.MobilePhone;
                }
                return personEntity;
            }
            catch (Exception) 
            {
                throw;
            }
        }
    }
}
