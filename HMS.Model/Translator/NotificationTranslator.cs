﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class NotificationTranslator : TranslatorBase<Notification, NOTIFICATION>
    {
        private PersonTranslator persontranslator;
        public NotificationTranslator() 
        {
            persontranslator = new PersonTranslator();
        }
        public override Notification TranslateToModel(NOTIFICATION notificationEntity)
        {
            try
            {
                Notification notification = null;
                if (notificationEntity != null)
                {
                    notification = new Notification();
                    notification.NotificationId = notificationEntity.Notification_Id;
                    notification.Message = notificationEntity.Message;
                    notification.Datetime = notificationEntity.Date_Time;
                    notification.Status = notificationEntity.Status;
                    notification.Subject = notificationEntity.Subject;
                    notification.Person = persontranslator.TranslateToModel(notificationEntity.PERSON);

                }
                return notification;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override NOTIFICATION TranslateToEntity(Notification notification)
        {
            try
            {
                NOTIFICATION notificationEntity = null;
                if (notification != null)
                {
                    notificationEntity = new NOTIFICATION();
                    notificationEntity.Notification_Id = notification.NotificationId;
                    notificationEntity.Message = notification.Message;
                    notificationEntity.Subject = notification.Subject;
                    notificationEntity.Status = notification.Status;
                    notificationEntity.Date_Time = notification.Datetime;
                    notificationEntity.Person_Id = notification.Person.PersonId;
                }
                return notificationEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
