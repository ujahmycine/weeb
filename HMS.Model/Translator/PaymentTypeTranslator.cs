﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class PaymentTypeTranslator : TranslatorBase<PaymentType, PAYMENT_TYPE>
    {
        public override PaymentType TranslateToModel(PAYMENT_TYPE paymentTypeEntity)
        {
            try
            {
                PaymentType paymentType = null;
                if (paymentTypeEntity != null)
                {
                    paymentType = new PaymentType();
                    paymentType.PaymentTypeId = paymentTypeEntity.Payment_Type_Id;
                    paymentType.Name = paymentTypeEntity.Name;
                    paymentType.Description = paymentTypeEntity.Description;
                }
                return paymentType;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PAYMENT_TYPE TranslateToEntity(PaymentType paymentType)
        {
            try
            {
                PAYMENT_TYPE paymentTypeEntity = null;
                if (paymentType != null)
                {
                    paymentTypeEntity = new PAYMENT_TYPE();
                    paymentTypeEntity.Payment_Type_Id = paymentType.PaymentTypeId;
                    paymentTypeEntity.Name = paymentType.Name;
                    paymentTypeEntity.Description = paymentType.Description;

                }
                return paymentTypeEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
