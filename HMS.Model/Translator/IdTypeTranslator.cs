﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class IdTypeTranslator : TranslatorBase<IdType, ID_TYPE>
    {
        public override IdType TranslateToModel(ID_TYPE idTypeEntity)
        {
            try
            {
                IdType idType = null;
                if (idTypeEntity != null)
                {
                    idType = new IdType();
                    idType.IdTypeId = idTypeEntity.Id_Type_Id;
                    idType.Name = idTypeEntity.Name;
                    idType.Active = idTypeEntity.Active;
                }
                return idType;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ID_TYPE TranslateToEntity(IdType idType)
        {
            try
            {
                ID_TYPE idTypeEntity = null;
                if (idType != null)
                {
                    idTypeEntity = new ID_TYPE();
                    idTypeEntity.Id_Type_Id = idType.IdTypeId;
                    idTypeEntity.Name = idType.Name;
                    idTypeEntity.Active = idType.Active;

                }
                return idTypeEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
