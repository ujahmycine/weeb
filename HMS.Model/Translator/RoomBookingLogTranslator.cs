﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class RoomBookingLogTranslator : TranslatorBase<RoomBookingLog, ROOM_BOOKING_LOG>
    {
        private PaymentTranslator paymentTranslator;
        private RoomBookingTranslator roomBookingTranslator;
        private RoomStatusTranslator roomStatusTranslator;
        public RoomBookingLogTranslator()
        {
            paymentTranslator = new PaymentTranslator();
            roomBookingTranslator = new RoomBookingTranslator();
            roomStatusTranslator = new RoomStatusTranslator();
        }
        public override RoomBookingLog TranslateToModel(ROOM_BOOKING_LOG Entity)
        {
            try
            {
                RoomBookingLog roomBookingLog = null;
                if (Entity != null)
                {
                    roomBookingLog = new RoomBookingLog();
                    roomBookingLog.Id = Entity.Id;
                    roomBookingLog.RoomBookings = roomBookingTranslator.TranslateToModel(Entity.ROOM_BOOKING);
                    roomBookingLog.Payment = paymentTranslator.TranslateToModel(Entity.PAYMENT);
                    roomBookingLog.BookingDate = Entity.Booking_Date;
                    roomBookingLog.NumOfNight = Entity.Num_Of_Night;
                    roomBookingLog.NumOfRooms = Entity.Num_Of_Rooms;
                    roomBookingLog.CheckInStatus = roomStatusTranslator.TranslateToModel(Entity.ROOM_STATUS);

                }
                return roomBookingLog;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override ROOM_BOOKING_LOG TranslateToEntity(RoomBookingLog model)
        {
            try
            {
                ROOM_BOOKING_LOG Entity = null;
                if (model != null)
                {
                    Entity = new ROOM_BOOKING_LOG();
                    Entity.Id = model.Id;
                    Entity.Room_Bookinh_Id = model.RoomBookings.RoomBookingId;
                    Entity.Payment_Id = model.Payment.PaymentId;
                    Entity.Booking_Date = model.BookingDate;
                    Entity.Num_Of_Night = model.NumOfNight;
                    Entity.Num_Of_Rooms = model.NumOfNight;
                    Entity.Check_In_Status_Id = model.CheckInStatus.RoomStatusId;

                }
                return Entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}