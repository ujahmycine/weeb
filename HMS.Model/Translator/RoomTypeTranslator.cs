﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    public class RoomTypeTranslator : TranslatorBase<RoomType, ROOM_TYPE>
    {
        public override RoomType TranslateToModel(ROOM_TYPE roomTypeEntity)
        {
            try
            {
                RoomType roomType = null;
                if (roomTypeEntity != null)
                {
                    roomType = new RoomType();
                    roomType.RoomTypeId = roomTypeEntity.Room_Type_Id;
                    roomType.Name = roomTypeEntity.Name;
                    roomType.Active = roomTypeEntity.Active;
                    roomType.Max_Discount = roomTypeEntity.Max_Discount;
                    roomType.DEscription = roomTypeEntity.Description;
                    roomType.RoomCharge = roomTypeEntity.RoomCharge;

                }
                return roomType;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ROOM_TYPE TranslateToEntity(RoomType roomType)
        {
            try
            {
                ROOM_TYPE roomtypeEntity = null;
                if (roomType != null)
                {
                    roomtypeEntity = new ROOM_TYPE();
                    roomtypeEntity.Room_Type_Id = roomType.RoomTypeId;
                    roomtypeEntity.Name = roomType.Name;
                    roomtypeEntity.Active = roomType.Active;
                    roomtypeEntity.Max_Discount = roomType.Max_Discount;
                    roomtypeEntity.Description = roomType.DEscription;
                    roomtypeEntity.RoomCharge = roomType.RoomCharge;
                }
                return roomtypeEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
