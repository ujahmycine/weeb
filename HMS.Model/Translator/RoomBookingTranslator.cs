﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class RoomBookingTranslator :TranslatorBase<RoomBookings,ROOM_BOOKING>
    {
        private RoomTypeTranslator roomTypeTranslator;
        private PersonTranslator personTranslator;
        private RoomStatusTranslator checkInStatusTranslator;
        private RoomTranslator roomTranslator;
      //  private PaymentTranslator paymentTranslator;
       
        public RoomBookingTranslator() 
        {
            roomTypeTranslator = new RoomTypeTranslator();
            personTranslator = new PersonTranslator();
            checkInStatusTranslator = new RoomStatusTranslator();
            roomTranslator = new RoomTranslator();
           // paymentTranslator = new PaymentTranslator();
        }

        public override RoomBookings TranslateToModel(ROOM_BOOKING bookingEntity) 
        {
            try 
            {
                RoomBookings booking = null;
                if (bookingEntity != null) 
                {
                    booking = new RoomBookings();
                    booking.RoomBookingId = bookingEntity.Room_Booking_Id;
                    booking.CheckInDate = bookingEntity.Check_In_Date;
                    booking.CheckOutDate = bookingEntity.Check_Out_Date;
                    booking.ArrivalTime = bookingEntity.Arrival_Time;
                    booking.DepatureTime = bookingEntity.Departure_Time;
                    booking.Notes = bookingEntity.Notes;
                    booking.Paid = bookingEntity.Paid;
                   
                    booking.BookingDate = bookingEntity.BookingDate;
                    booking.RoomType = roomTypeTranslator.TranslateToModel(bookingEntity.ROOM_TYPE);
                    booking.Person = personTranslator.TranslateToModel(bookingEntity.PERSON);
                    booking.Room = roomTranslator.TranslateToModel(bookingEntity.ROOM);
                    booking.NumOfNight = bookingEntity.Num_Of_Nights;
                    booking.NumOfRooms = bookingEntity.Number_Of_Rooms;
                    booking.CheckInStatus = checkInStatusTranslator.TranslateToModel(bookingEntity.ROOM_STATUS);
                   
                }
                return booking;
            }
            catch (Exception ) 
            {
                throw;
            }
        }
        public override ROOM_BOOKING TranslateToEntity(RoomBookings booking) 
        {
            try 
            {
                ROOM_BOOKING bookingEntity = null;
                if (booking != null) 
                {
                    bookingEntity = new ROOM_BOOKING();
                    bookingEntity.Room_Booking_Id = booking.RoomBookingId;
                    bookingEntity.Check_In_Date = booking.CheckInDate;
                    bookingEntity.Check_Out_Date = booking.CheckOutDate;
                    bookingEntity.Arrival_Time = booking.ArrivalTime;
                    bookingEntity.Departure_Time = booking.DepatureTime;
                    bookingEntity.Notes = booking.Notes;
                    bookingEntity.RoomType_Id = booking.RoomType.RoomTypeId;
                    bookingEntity.Person_Id = booking.Person.PersonId;
                    bookingEntity.Paid = booking.Paid;
                   
                    bookingEntity.BookingDate = booking.BookingDate;
                    bookingEntity.Num_Of_Nights = booking.NumOfNight;
                    bookingEntity.Number_Of_Rooms = booking.NumOfRooms;
                    bookingEntity.Room_Id = 1;
                    bookingEntity.Check_In_Status_Id = booking.CheckInStatus.RoomStatusId;
                }
                return bookingEntity;
            }
            catch (Exception) 
            {
                throw;
            }
        }
    }
}
