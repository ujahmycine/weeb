﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Translator
{
    
   public class RoomTranslator:TranslatorBase<Room,ROOM>
    {
       private RoomTypeTranslator roomTypeTranslator;
       private RoomStatusTranslator roomStatusTranslator;
       public RoomTranslator() 
       {
           roomTypeTranslator = new RoomTypeTranslator();
           roomStatusTranslator = new RoomStatusTranslator();
       }
       public override Room TranslateToModel(ROOM roomEntity) 
       {
           try 
           {
               Room room = null;
               if (roomEntity != null) 
               {
                   room = new Room();
                   room.RoomId = roomEntity.Room_Id;
                   room.Name = roomEntity.Name;
                   room.Active = roomEntity.Active;
                   room.Description = roomEntity.Description;
                   room.Max_Occupant = roomEntity.Max_Occupant;
                   room.MaxAdult = roomEntity.Max_Adult;
                    room.NoOfChildren = roomEntity.No_Of_Children;
                   room.RoomStatus = roomStatusTranslator.TranslateToModel(roomEntity.ROOM_STATUS);
                   room.RoomType = roomTypeTranslator.TranslateToModel(roomEntity.ROOM_TYPE);
                  
               }
               return room;
           }
           catch (Exception) 
           {
               throw;
           }
       }
       public override ROOM TranslateToEntity(Room room) 
       {
           try 
           {
               ROOM roomEntity = null;
               if (room != null) 
               {
                   roomEntity = new ROOM();
                   roomEntity.Room_Id = room.RoomId;
                   roomEntity.Name = room.Name;
                   roomEntity.Description = room.Description;
                   roomEntity.Active = room.Active;
                   roomEntity.Max_Adult = room.MaxAdult;
                   roomEntity.Max_Occupant = room.Max_Occupant;
                   roomEntity.Room_Status_Id = room.RoomStatus.RoomStatusId;
                   roomEntity.Room_Type_Id = room.RoomType.RoomTypeId;
                    roomEntity.No_Of_Children = room.NoOfChildren;
               }
               return roomEntity;
           }
           catch (Exception) 
           {
               throw;
           }
       }
    }
}
