﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class PaymentTranslator:TranslatorBase<Payment,PAYMENT>
    {
        private PersonTranslator personTranslator;
       
        private PaymentTypeTranslator paymentTypeTranslator;
        private ServiceTranslator serviceTranslator;
        private RoomBookingTranslator roomBookingTranslator;

        public PaymentTranslator() 
        {
            personTranslator = new PersonTranslator();
            serviceTranslator = new ServiceTranslator();
            paymentTypeTranslator = new PaymentTypeTranslator();
            roomBookingTranslator = new RoomBookingTranslator();
        }

        public override Payment TranslateToModel(PAYMENT paymentEntity) 
        {
            try 
            {
                Payment payment = null;
                if (paymentEntity != null) 
                {
                    payment = new Payment();
                    payment.PaymentId = paymentEntity.Payment_Id;
                    payment.Amount = paymentEntity.Amount;
                    payment.Description = paymentEntity.Description;
                    payment.PaymentGateWay = paymentEntity.Payment_Gateway;
                    payment.DatePaid = paymentEntity.Date_Booked;

                    payment.PaymentType = paymentTypeTranslator.TranslateToModel(paymentEntity.PAYMENT_TYPE);
                    payment.Service = serviceTranslator.TranslateToModel(paymentEntity.SERVICE);
                    payment.Person = personTranslator.TranslateToModel(paymentEntity.PERSON);
                    payment.Pin = paymentEntity.Pin;
                    payment.RoomBookings = roomBookingTranslator.TranslateToModel(paymentEntity.ROOM_BOOKING);
                }
                return payment;
            }
            catch(Exception)
            {
                throw;
            }
        }
        public override PAYMENT TranslateToEntity(Payment payment) 
        {
            try 
            {
                PAYMENT paymentEntity = null;
                if (payment != null) 
                {
                    paymentEntity = new PAYMENT();
                    paymentEntity.Payment_Id = payment.PaymentId;
                    paymentEntity.Amount = payment.Amount;
                    paymentEntity.Description = payment.Description;
                    paymentEntity.Date_Booked = payment.DatePaid;
                    paymentEntity.Payment_Gateway = payment.PaymentGateWay;
                    paymentEntity.Service_Id = payment.Service.ServiceId;
                    paymentEntity.Person_Id = payment.Person.PersonId;
                    paymentEntity.Payment_Type_Id = payment.PaymentType.PaymentTypeId;
                    paymentEntity.Pin = payment.Pin;
                    paymentEntity.Room_Booking_Id = payment.RoomBookings.RoomBookingId;
                    
                }
                return paymentEntity;
            }
            catch (Exception) 
            {
                throw;
            }
        }
    }
}
