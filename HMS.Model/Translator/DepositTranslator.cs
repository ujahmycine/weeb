﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class DepositTranslator : TranslatorBase<Deposit, DEPOSIT>
    {
        private RoomBookingTranslator roomBookingTranslator;
        public DepositTranslator() 
        {
            roomBookingTranslator = new RoomBookingTranslator();
        }
        public override Deposit TranslateToModel(DEPOSIT depositEntity)
        {
            try
            {
                Deposit deposit = null;
                if (depositEntity != null)
                {
                    deposit = new Deposit();
                    deposit.DepositId = depositEntity.Deposit_Id;
                    deposit.DEscription = depositEntity.Description;
                    deposit.RoomBooking = roomBookingTranslator.TranslateToModel(depositEntity.ROOM_BOOKING);
                }
                return deposit;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override DEPOSIT TranslateToEntity(Deposit deposit)
        {
            try
            {
                DEPOSIT depositEntity = null;
                if (deposit != null)
                {
                    depositEntity = new DEPOSIT();
                    depositEntity.Deposit_Id = deposit.DepositId;
                    depositEntity.Description = deposit.DEscription;
                    depositEntity.Room_Booking_Id = deposit.RoomBooking.RoomBookingId   ;

                }
                return depositEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
