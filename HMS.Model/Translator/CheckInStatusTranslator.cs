﻿using HMS.Model.Entity;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Translator
{
    public class CheckInStatusTranslator : TranslatorBase<CheckInStatus, CHECK_IN_STATUS>
    {
        public override CheckInStatus TranslateToModel(CHECK_IN_STATUS idTypeEntity)
        {
            try
            {
                CheckInStatus idType = null;
                if (idTypeEntity != null)
                {
                    idType = new CheckInStatus();
                    idType.Id = idTypeEntity.Check_In_Status_Id;
                    idType.Name = idTypeEntity.Name;
                   
                }
                return idType;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override CHECK_IN_STATUS TranslateToEntity(CheckInStatus idType)
        {
            try
            {
                CHECK_IN_STATUS idTypeEntity = null;
                if (idType != null)
                {
                    idTypeEntity = new CHECK_IN_STATUS();
                    idTypeEntity.Check_In_Status_Id = idType.Id;
                    idTypeEntity.Name = idType.Name;
                  
                }
                return idTypeEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
