﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace HMS.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Reservation()
        {
            AdminViewModel viewmodel = new AdminViewModel();
            List<Room> room = new List<Room>();
            RoomLogic roomLogic = new RoomLogic();
            List<RoomType> roomType = new List<RoomType>();
            RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
            room = roomLogic.GetModelsBy(r => r.Max_Adult >= 0 && r.No_Of_Children >= 0 && r.ROOM_STATUS.Room_Status_Id == 2);
            roomType = roomTypeLogic.GetAll();
            //ViewBag.Arival = viewmodel.RoomBookings.ArrivalTime.ToString("d");
            //ViewBag.NUmOfNight = viewmodel.RoomBookings.NumOfNight;
            //ViewBag.MaxAdult = viewmodel.Room.MaxAdult;
            //ViewBag.NumOfChildren = viewmodel.Room.NoOfChildren;
            //ViewBag.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
            //int days = (int)viewmodel.RoomBookings.NumOfNight - 1;
            //viewmodel.RoomBookings.DepatureTime = viewmodel.RoomBookings.ArrivalTime.AddDays(days);
            //ViewBag.Departure = viewmodel.RoomBookings.DepatureTime.ToString("d");
            //TempData["roomtype"] = viewmodel;
            viewmodel.Rooms = room;
            viewmodel.RoomTypes = roomType;
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult YourReservation(DateTime ArivalDate, int Night, int adult, int children, int Room)
        {

            try
            {
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                List<RoomType> roomType = new List<RoomType>();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                AdminViewModel viewmodel = new AdminViewModel();
                // room = roomLogic.GetModelsBy(r => r.Max_Adult >= viewmodel.Room.MaxAdult && r.No_Of_Children >= viewmodel.Room.NoOfChildren && r.ROOM_STATUS.Room_Status_Id == 2);
                roomType = roomTypeLogic.GetAll();
                ViewBag.Arival = ArivalDate.ToString("d");
                ViewBag.NUmOfNight = Night;
                ViewBag.MaxAdult = adult;
                ViewBag.NumOfChildren = children;
                ViewBag.NumOfRooms = Room;
                //int days = (int)Night;
                //viewmodel.RoomBookings.DepatureTime = ArivalDate.AddDays(days);
                //ViewBag.Departure = viewmodel.RoomBookings.DepatureTime.ToString("d");
                viewmodel.RoomBookings.ArrivalTime = ArivalDate.Date;
                viewmodel.RoomBookings.NumOfNight = ViewBag.NUmOfNight;
                viewmodel.Room.MaxAdult = ViewBag.MaxAdult;
                viewmodel.Room.NoOfChildren = ViewBag.NumOfChildren;
                viewmodel.RoomBookings.NumOfRooms = ViewBag.NumOfRooms;
                //viewmodel.RoomBookings.NumOfNight = ViewBag.NUmOfNight;
                viewmodel.RoomBookings.NumOfRooms = Room;
                int days = (int)viewmodel.RoomBookings.NumOfNight;
                viewmodel.RoomBookings.DepatureTime = ArivalDate.Date.AddDays(days).Date;
                ViewBag.Departure = viewmodel.RoomBookings.DepatureTime.ToString("d");
                viewmodel.RoomTypes = roomType;
                TempData["roomtype"] = viewmodel;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult Reservation(AdminViewModel viewmodel)
            {
            try
            {
                AdminViewModel viewmodels = new AdminViewModel();
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                List<RoomType> roomType = new List<RoomType>();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                room = roomLogic.GetModelsBy(r => r.Max_Adult >= viewmodel.Room.MaxAdult && r.No_Of_Children >= viewmodel.Room.NoOfChildren && r.ROOM_STATUS.Room_Status_Id == 2);
                roomType = roomTypeLogic.GetAll();
                ViewBag.Arival = viewmodel.RoomBookings.ArrivalTime.ToString("d");
                ViewBag.NUmOfNight = viewmodel.RoomBookings.NumOfNight;
                ViewBag.MaxAdult = viewmodel.Room.MaxAdult;
                ViewBag.NumOfChildren = viewmodel.Room.NoOfChildren;
                ViewBag.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
                int days = (int)viewmodel.RoomBookings.NumOfNight - 1;
                viewmodel.RoomBookings.DepatureTime = viewmodel.RoomBookings.ArrivalTime.AddDays(days);
                ViewBag.Departure = viewmodel.RoomBookings.DepatureTime.ToString("d");
                TempData["roomtype"] = viewmodel;
                viewmodel.RoomTypes = roomType;
                viewmodel.Rooms = room;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        //public ActionResult MakePayment(AdminViewModel viewmodel)
        //{
        //    return View();
        //}
        public ActionResult RoomBook(int id)
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                AdminViewModel viewmodel = new AdminViewModel();
                Room room = new Room();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                RoomLogic roomLogic = new RoomLogic();
                viewmodel = (AdminViewModel)TempData["roomtype"];
                roomType = roomTypeLogic.GetModelBy(r => r.Room_Type_Id == id);
                viewmodel.RoomType = new RoomType();
                viewmodel.RoomType = roomType;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                SetMessage("Make a new Selection!", Message.Category.Error);
                return RedirectToAction("Index","Home");
            }
            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
            ViewBag.Country = Utility.PopulateCountrySelectList();
            ViewBag.Gender = Utility.PopulateGenderSelectList();
            return View();
        }
        [HttpPost]
        public ActionResult RoomBook(AdminViewModel viewmodel)
        {
            try
            {

                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                Payment payment = new Payment();
                RoomBookings roomBooking = new RoomBookings();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                Payment NewPay = new Payment();
                roomType = roomTypeLogic.GetModelBy(r=>r.Room_Type_Id == viewmodel.RoomType.RoomTypeId);
                if ((viewmodel.RoomBookings.ArrivalTime.Date < DateTime.Now.Date))
                {
                    SetMessage("Invalid Check In Date! Date entered has to be today and above", Message.Category.Error);
                    ViewBag.PaymentType = Utility.PopulatePaymentTypeSelectList();
                    ViewBag.Service = Utility.PopulateServiceSelectList();
                    return RedirectToAction("Index","Home");
                }
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    //create person
                    person = CreatePerson(viewmodel);
                    Person NewPerson = personLogic.GetModelBy(p=>p.Person_Id == person.PersonId);
                    

                    //create booking
                    roomBooking.Person = new Person { PersonId = person.PersonId };
                    roomBooking.RoomType = new RoomType { RoomTypeId = viewmodel.RoomType.RoomTypeId };
                    roomBooking.ArrivalTime = viewmodel.RoomBookings.ArrivalTime.Date;
                    int days = (int)viewmodel.RoomBookings.NumOfNight - 1;
                    viewmodel.RoomBookings.DepatureTime = viewmodel.RoomBookings.ArrivalTime.AddDays(days);
                    roomBooking.DepatureTime = viewmodel.RoomBookings.DepatureTime.Date;
                    roomBooking.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
                    roomBooking.BookingDate = DateTime.Now;
                    roomBooking.NumOfNight = viewmodel.RoomBookings.NumOfNight;
                    roomBooking.CheckInDate = DateTime.Now.Date;
                    roomBooking.CheckOutDate = DateTime.Now.Date;
                    roomBooking.CheckInStatus = new RoomStatus { RoomStatusId = 5 };
                    RoomBookings roomBookings = new RoomBookings();
                    roomBookings = CreateRoomBooking(roomBooking);
                    RoomBookings NewRoomBooking = roomBookingLogic.GetModelBy(r=>r.Room_Booking_Id == roomBookings.RoomBookingId);
                    //create payment
                    payment.Person = new Person { PersonId = person.PersonId };
                    payment.Amount = NewRoomBooking.RoomType.RoomCharge * viewmodel.RoomBookings.NumOfNight * viewmodel.RoomBookings.NumOfRooms;
                    payment.Description = "Room Reservation Payment made" + " On " + DateTime.Now;
                    payment.Service = new Service { ServiceId = 4 };
                    payment.DatePaid = DateTime.Now;
                    
                    payment.PaymentType = new PaymentType { PaymentTypeId = 2 };
                    payment.RoomBookings = new RoomBookings { RoomBookingId = NewRoomBooking.RoomBookingId };

                    //Update Room
                    //UpdateRoom(room);
                    Payment NewPayment = CreatePayment(payment);

                     NewPay = paymentLogic.GetModelBy(p=>p.Payment_Id == NewPayment.PaymentId);


                    string PaystackSecrect = ConfigurationManager.AppSettings["PaystackSecrect"].ToString();
                    string PaystackSubAccount = ConfigurationManager.AppSettings["PaystackSubAccount"].ToString();

                    if (!String.IsNullOrEmpty(PaystackSecrect))
                    {
                        PaystackLogic paystackLogic = new PaystackLogic();
                        NewPay.Person = NewPerson;
                        viewmodel.Paystack = paystackLogic.GetBy(NewPay);
                        int transactionCharge = 0;
                        if (viewmodel.Paystack == null)
                        {
                             transactionCharge = Convert.ToInt32(NewPay.Amount);

                        }
                        PaystackRepsonse paystackRepsonse = paystackLogic.MakePayment(payment, PaystackSecrect, PaystackSubAccount, transactionCharge);
                        if (paystackRepsonse != null && paystackRepsonse.status && !String.IsNullOrEmpty(paystackRepsonse.data.authorization_url))
                        {
                            paystackRepsonse.Paystack = new Paystack();
                            paystackRepsonse.Paystack.Payment = NewPay;
                            paystackRepsonse.Paystack.authorization_url = paystackRepsonse.data.authorization_url;
                            paystackRepsonse.Paystack.access_code = paystackRepsonse.data.access_code;
                            viewmodel.Paystack = paystackLogic.Create(paystackRepsonse.Paystack);

                        }
                    }
                    //send mail
                    int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    string password = ConfigurationManager.AppSettings["password"];
                    string from = ConfigurationManager.AppSettings["from"];
                    string host = ConfigurationManager.AppSettings["host"];
                    Utility utiz = new Utility();
                    utiz.SendMail(NewPerson.Email, "Hello " + NewPerson.FullName + " Your Reservation <br/> Pament Details<br/>Reference Number:"+NewPay.Pin+ "<br/>Payment Amount:" +NewPay.Amount+ " <br/> Booking Date: " +NewRoomBooking.BookingDate+" Enjoy your Stay At Lloydant Hotels", "Reservation Details ", from, password, host, port);


                    transaction.Complete();

                }
                SetMessage("Payment Detail has been sent to your Email Address! Please check your email for your reference number",Message.Category.Information);
                return RedirectToAction("PaymentDetails","Home", new { refNo =NewPay.Pin });

            }
            catch (Exception ex)
            {
                
                SetMessage("Make Sure You are Connected To The Internet!"+ex,Message.Category.Error);
                return RedirectToAction("Reservation","Home");
            }
            return View();
        }

       

        public ActionResult PaymentDetails(string refNo)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                Paystack paystack = new Paystack();
                PaystackLogic paystackLogic = new PaystackLogic();
                payment = paymentLogic.GetModelBy(p => p.Pin == refNo);
                paystack = paystackLogic.GetModelBy(p => p.Payment_Id == payment.PaymentId);
                AdminViewModel viewmodel = new AdminViewModel();
                viewmodel.Payment = payment;
                viewmodel.Paystack = paystack;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                SetMessage("Make a new Selection!", Message.Category.Error);
                return RedirectToAction("Reservation", "Home");
            }
           
            
        }
        
        public ActionResult NewReservation()
        {
            List<Room> room = new List<Room>();
            room = (List<Room>)TempData["room"];
            return View(room);
        }
        public ActionResult RoomReserve(int id)
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                    ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                AdminViewModel viewmodel = new AdminViewModel();
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                viewmodel = (AdminViewModel)TempData["roomtype"];
                roomType = roomTypeLogic.GetModelBy(r => r.Room_Type_Id == id);
                viewmodel.RoomType = roomType;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!", Message.Category.Error);
                
            }
            return View();
        }
        [HttpPost]
        public ActionResult RoomReserve(AdminViewModel viewmodel)
        {
            try
            {
                try
                {

                    ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                    ViewBag.Country = Utility.PopulateCountrySelectList();
                    ViewBag.Gender = Utility.PopulateGenderSelectList();
                    PaymentLogic paymentLogic = new PaymentLogic();
                    RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                    RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                    Payment payment = new Payment();
                    RoomBookings roomBooking = new RoomBookings();
                    RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                    RoomType roomType = new RoomType();
                    RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                    Person person = new Person();
                    PersonLogic personLogic = new PersonLogic();
                    Payment NewPay = new Payment();
                    roomType = roomTypeLogic.GetModelBy(r => r.Room_Type_Id == viewmodel.RoomType.RoomTypeId);
                    //if ((viewmodel.RoomBookings.ArrivalTime.Day < DateTime.Now.Day))
                    //{
                    //    SetMessage("Invalid Check In Date! Date entered has to be today and above", Message.Category.Error);
                    //    ViewBag.PaymentType = Utility.PopulatePaymentTypeSelectList();
                    //    ViewBag.Service = Utility.PopulateServiceSelectList();
                    //    return View();
                    //}
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                    {
                        //create person
                        person = CreatePerson(viewmodel);
                        Person NewPerson = personLogic.GetModelBy(p => p.Person_Id == person.PersonId);


                        //create booking
                        roomBooking.Person = new Person { PersonId = person.PersonId };
                        roomBooking.RoomType = new RoomType { RoomTypeId = viewmodel.RoomType.RoomTypeId };
                        roomBooking.ArrivalTime = viewmodel.RoomBookings.ArrivalTime;
                        roomBooking.BookingDate = DateTime.Now;
                        roomBooking.NumOfNight = viewmodel.RoomBookings.NumOfNight;
                        roomBooking.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
                        roomBooking.CheckInDate = DateTime.Now;
                        roomBooking.CheckOutDate = DateTime.Now;
                        roomBooking.DepatureTime = viewmodel.RoomBookings.DepatureTime;
                        roomBooking.CheckInStatus = new RoomStatus { RoomStatusId = 5 };
                        RoomBookings roomBookings = new RoomBookings();
                        roomBookings = CreateRoomBooking(roomBooking);
                        RoomBookings NewRoomBooking = roomBookingLogic.GetModelBy(r => r.Room_Booking_Id == roomBookings.RoomBookingId);
                        //create payment
                        payment.Person = new Person { PersonId = person.PersonId };
                        payment.Amount = NewRoomBooking.RoomType.RoomCharge * viewmodel.RoomBookings.NumOfNight * viewmodel.RoomBookings.NumOfRooms;
                        payment.Description = "Room Reservation without Payment" + " On " + DateTime.Now;
                        payment.Service = new Service { ServiceId = 4 };
                        payment.DatePaid = DateTime.Now;
                        payment.PaymentType = new PaymentType { PaymentTypeId = 2 };
                        payment.RoomBookings = new RoomBookings { RoomBookingId = NewRoomBooking.RoomBookingId };

                        //Update Room
                        // UpdateRoom(room);
                        Payment NewPayment = CreatePayment(payment);

                        //send mail
                        int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                        string password = ConfigurationManager.AppSettings["password"];
                        string from = ConfigurationManager.AppSettings["from"];
                        string host = ConfigurationManager.AppSettings["host"];
                        Utility utiz = new Utility();
                        utiz.SendMail(NewPerson.Email, "Hello " + NewPerson.FullName + " Your Reservation <br/> Pament Details<br/>Reference Number:" + NewPayment.Pin + "<br/>Payment Amount:" + NewPayment.Amount + " <br/> Booking Date: " + NewRoomBooking.BookingDate + " Room reservation last for 2 hours, make your payment on time! Enjoy your Stay At Lloydant Hotels", "Reservation Details ", from, password, host, port);

                        transaction.Complete();

                    }
                    SetMessage("Room Reserved Successfully, Check Your mail for you reference number!", Message.Category.Information);
                    return RedirectToAction("Reservation", "Home");

                }
                catch (Exception ex)
                {
                    SetMessage("Make Sure You are Connected To The Internet!"+ex, Message.Category.Information);
                    return RedirectToAction("Reservation", "Home");
                }
                return View();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        private Payment CreatePayment(Payment payment)
        {
            Payment payments = new Payment();
            PaymentLogic paymentLogic = new PaymentLogic();
            RoomType roomType = new RoomType();
            RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
            Service service = new Service();
            ServiceLogic serviceLogic = new ServiceLogic();
            Person person = new Person();
            PersonLogic personLogic = new PersonLogic();
            PaymentType paymentType = new PaymentType();
            PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
            Room room = new Room();
            RoomLogic roomLogic = new RoomLogic();
            payments = paymentLogic.Create(payment);
            return payments;
        }

        private RoomBookings CreateRoomBooking(RoomBookings roomBooking)
        {
            try
            {
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBookings = new RoomBookings();
                roomBookings = roomBookingLogic.Create(roomBooking);
                return roomBookings;
            }
            catch (Exception ex)
                {

                throw ex;
            }
           
        }
        private Person CreatePerson(AdminViewModel viewmodel)
        {
            try
            {
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
               
                person = viewmodel.Person;
               
                Person newPerson = personLogic.Create(person);
                return newPerson;

            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult ReservedRooms()
        {

            return View();
        }
        [HttpPost]
        public ActionResult ReservedRooms(AdminViewModel viewModel)
        {
            try
            {
                return RedirectToAction("ViewReservation", "Home",new { refNo = viewModel.Payment.Pin});
            }
            catch (Exception ex)
            {
                throw ex;   
            }
            
        }
        public ActionResult ViewReservation(string refNo)
        {
            try
            {
                ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
                AdminViewModel viewmodel = new AdminViewModel();
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookings roomBookings = new RoomBookings();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                Paystack paystack = new Paystack();
                PaystackLogic paystackLogic = new PaystackLogic();
                payment = paymentLogic.GetModelBy(p => p.Pin == refNo);
                TempData["payment"] = payment;
                paystack = paystackLogic.GetModelBy(p=>p.Payment_Id == payment.PaymentId);
                if (paystack == null)
                {
                    return RedirectToAction("MakePayment", "Home");
                }
                payment = paystack.Payment;
                viewmodel.Payment = payment;
                roomBookings = roomBookingLogic.GetModelBy(b => b.Room_Booking_Id == payment.RoomBookings.RoomBookingId);
                viewmodel.RoomBookings = roomBookings;
                viewmodel.Paystack = paystack;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                SetMessage("No record tied to the reference number", Message.Category.Error);
                return RedirectToAction("ReservedRooms","Home");
            }
        }

        public ActionResult MakePayment()
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                AdminViewModel viewModel = new AdminViewModel();
                Payment payment = new Payment();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBookings = new RoomBookings();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                payment = (Payment)TempData["payment"];
                roomBookings = roomBookingLogic.GetModelBy(r=>r.Room_Booking_Id == payment.RoomBookings.RoomBookingId);
                person = personLogic.GetModelBy(p => p.Person_Id == payment.Person.PersonId);
                viewModel.Payment = payment;
                viewModel.RoomBookings = roomBookings;
                viewModel.Person = person;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occur",Message.Category.Error);
                return View();
            }
            
        }
        [HttpPost]
        public ActionResult MakePayment(AdminViewModel viewmodel)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookings roomBookings = new RoomBookings();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                payment = paymentLogic.GetModelBy(p => p.Payment_Id == viewmodel.Payment.PaymentId);
                // payment.Person = viewmodel.Person;
                roomBookings = roomBookingLogic.GetModelBy(r => r.Room_Booking_Id == viewmodel.RoomBookings.RoomBookingId);
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    string PaystackSecrect = ConfigurationManager.AppSettings["PaystackSecrect"].ToString();
                    string PaystackSubAccount = ConfigurationManager.AppSettings["PaystackSubAccount"].ToString();

                    if (!String.IsNullOrEmpty(PaystackSecrect))
                    {
                        PaystackLogic paystackLogic = new PaystackLogic();
                        payment.Person = viewmodel.Person;
                        viewmodel.Paystack = paystackLogic.GetBy(payment);
                        int transactionCharge = 0;
                        if (viewmodel.Paystack == null)
                        {
                            transactionCharge = Convert.ToInt32(payment.Amount);

                        }
                        PaystackRepsonse paystackRepsonse = paystackLogic.MakePayment(payment, PaystackSecrect, PaystackSubAccount, transactionCharge);
                        if (paystackRepsonse != null && paystackRepsonse.status && !String.IsNullOrEmpty(paystackRepsonse.data.authorization_url))
                        {
                            paystackRepsonse.Paystack = new Paystack();
                            paystackRepsonse.Paystack.Payment = payment;
                            paystackRepsonse.Paystack.authorization_url = paystackRepsonse.data.authorization_url;
                            paystackRepsonse.Paystack.access_code = paystackRepsonse.data.access_code;
                            viewmodel.Paystack = paystackLogic.Create(paystackRepsonse.Paystack);

                        }
                    }

                    //send mail
                    int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    string password = ConfigurationManager.AppSettings["password"];
                    string from = ConfigurationManager.AppSettings["from"];
                    string host = ConfigurationManager.AppSettings["host"];
                    Utility utiz = new Utility();
                    utiz.SendMail(payment.Person.Email, "Hello " + payment.Person.FullName + " Your Reservation <br/> Pament Details<br/>Reference Number:" + payment.Pin + "<br/>Payment Amount:" + payment.Amount + " <br/> Booking Date: " + roomBookings.BookingDate + " Room reservation last for 2 hours, make your payment on time! Enjoy your Stay At Lloydant Hotels", "Reservation Details ", from, password, host, port);
                    transaction.Complete();
                }
                SetMessage("Payment Detail has been sent to your Email Address! Please check your email for your reference number", Message.Category.Information);
                return RedirectToAction("PaymentDetails", "Home", new { refNo = payment.Pin });

            }
            catch (Exception ex)
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                SetMessage("Check Your Internet connection!<br>"+ ex,Message.Category.Error);
                return RedirectToAction("ReservedRooms","Home");
            }
           
        }
        public ActionResult UpdateRoomBookings(AdminViewModel viewmodel)
        {
            Payment payment = new Payment();
            try
            {
                
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBookings = new RoomBookings();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                roomBookings = roomBookingLogic.GetModelBy(r => r.Room_Booking_Id == viewmodel.RoomBookings.RoomBookingId);
                payment = paymentLogic.GetModelBy(p=>p.Payment_Id == viewmodel.Payment.PaymentId);
                roomType = roomTypeLogic.GetModelBy(r=>r.Room_Type_Id == viewmodel.RoomBookings.RoomType.RoomTypeId);
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    //update Room Booking
                    roomBookings.RoomType = new RoomType { RoomTypeId = viewmodel.RoomBookings.RoomType.RoomTypeId };
                    roomBookings.BookingDate = viewmodel.RoomBookings.BookingDate;
                    roomBookings.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
                    roomBookings.NumOfNight = viewmodel.RoomBookings.NumOfNight;
                    UpdateRoomBookingsDetails(roomBookings);

                    //Update Payment
                    payment.Amount = roomType.RoomCharge * viewmodel.RoomBookings.NumOfNight * viewmodel.RoomBookings.NumOfRooms;
                    payment.Description = "Room Reservation Update on "+ DateTime.Now;
                    UpdatePaymentDetails(payment);

                    //send mail
                    int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    string password = ConfigurationManager.AppSettings["password"];
                    string from = ConfigurationManager.AppSettings["from"];
                    string host = ConfigurationManager.AppSettings["host"];
                    Utility utiz = new Utility();
                    utiz.SendMail(payment.Person.Email, "Hello " + payment.Person.FullName + "Reservation <br/> Update<br/>Reference Number:" + payment.Pin + "<br/>Payment Amount:" + payment.Amount + " <br/> Booking Date: " + roomBookings.BookingDate + " Room reservation last for 2 hours, make your payment on time! Enjoy your Stay At Lloydant Hotels", "Reservation Details ", from, password, host, port);

                    transaction.Complete();
                }
                SetMessage("Reservation Updated Successfully", Message.Category.Information);
                return RedirectToAction("Index","Home");
            }
            catch (Exception ex)
            {
                SetMessage("Error"+ex, Message.Category.Information);
                return View("UpdateRoomBookings","Booking", new { refNo = payment.Pin});
                //throw ex;
            }
           
        }

        private bool UpdatePaymentDetails(Payment payment)
        {
            PaymentLogic paymentLogic = new PaymentLogic();
            if (paymentLogic.UpdatePayment(payment))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool UpdateRoom(Room room)
        {
            RoomLogic roomLogic = new RoomLogic();
            if (roomLogic.UpdateRoomStatusToCheckedOut(room))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool UpdateRoomBookingsDetails(RoomBookings roomBookings)
        {
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            if (roomBookingLogic.UpdateRoomBookingCheckInStatus(roomBookings))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        

    }
}