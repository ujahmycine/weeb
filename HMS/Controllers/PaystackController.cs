﻿using HMS.Business;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Abundance_Nk.Web.Areas.Common.Controllers
{
    [AllowAnonymous]
    public class PaystackController : Controller
    {
       
        public ActionResult Index(Paystack paystack)
        {
            string PaystackSecrect = ConfigurationManager.AppSettings["PaystackSecrect"].ToString();
            PaystackLogic paystackLogic = new PaystackLogic();
            paystackLogic.VerifyPayment(new Payment() { Pin = paystack.reference }, PaystackSecrect);
            return RedirectToAction("Index", "Home", new { Area = "" });
        }
    }
}