﻿using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMS.Areas.Customers.Models
{
    public class CustomerViewModel
    {
        public Room Room { get; set; }
        public User User { get; set; }
        public RoomBookings RoomBooking { get; set; }
        public Payment Payment { get; set; }
        public string InvoiceNumber { get; set; }
        public List<Payment> PaymentList { get; set; }
        public List<Room> RoomList { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public TimeSpan TimeIn { get; set; }

        public TimeSpan TimeOut { get; set; }
        public RoomBookingPayment RoomBookingPayment { get; set; }
        public List<RoomBookings> ListOfbookings = new List<RoomBookings>();
    }
}