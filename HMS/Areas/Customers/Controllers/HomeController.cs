﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMS.Models;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HMS.Areas.Customers.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        // GET: Customers/Home
        private User _User;
        private UserLogic userLogic;
        public HomeController()
        {
            try
            {
                if (System.Web.HttpContext.Current.Session["user"] != null)
                {
                    userLogic = new UserLogic();
                    _User = System.Web.HttpContext.Current.Session["user"] as User;
                    _User = userLogic.GetBy(_User.UserId);
                }
                else
                {
                    FormsAuthentication.SignOut();
                    System.Web.HttpContext.Current.Response.Redirect("/Security/Account/Login");

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult ListOfBooking(int id)
        {
            try
            {
                List<RoomBookings> booking = new List<RoomBookings>();
                RoomBookingLogic bookingLogic = new RoomBookingLogic();
                booking = bookingLogic.GetModelsBy(b => b.Person_Id == id);
                return View(booking);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult GuestBooking()
        {
            return View();
        }
        public ActionResult Basic(int id)
        {
            try
            {
                if (_User != null)
                {
                    List<Room> room = new List<Room>();
                    RoomLogic roomLogic = new RoomLogic();
                    Person person = new Person();
                    PersonLogic personLogic = new PersonLogic();
                    User user = new User();
                    UserLogic userLogic = new UserLogic();
                    person = personLogic.GetModelBy(p=>p.Person_Id == _User.Person.PersonId);
                    if (person.MobilePhone == null)
                    {
                        return RedirectToAction("UpdateDetails","Home", new { pid = person.PersonId});
                    }
                    //if (person == null)
                    //{
                    //    return RedirectToAction("BookGuest","Home");
                    //}
                    room = roomLogic.GetModelsBy(r => r.Room_Id == id);

                    return RedirectToAction("RoomBook", "Booking", new { Area = "Customers", cid = _User.UserId, rid = id });
                }
                else
                {
                    FormsAuthentication.SignOut();
                    RedirectToAction("Login", "Account", new { Area = "Security" });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
           
        }
        public ActionResult UpdateDetails(int pid)
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();

                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                person = personLogic.GetModelBy(p => p.Person_Id == pid);
                LoginViewModel viewmodel = new LoginViewModel();
                viewmodel.Person = person;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        public ActionResult BookGuest()
        {
            //Room room = new Room();
            //RoomLogic roomLogic = new RoomLogic();

            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
            ViewBag.Country = Utility.PopulateCountrySelectList();
            ViewBag.Gender = Utility.PopulateGenderSelectList();
            //room = roomLogic.GetModelBy(r => r.Room_Type_Id == rid);
            
            return View();
        }
        [HttpPost]
        public ActionResult BookGuest(LoginViewModel viewmodel)
        {
            try
            {
                //int id = TempData["id"];
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                
                person = personLogic.Create(viewmodel.Person);
                return RedirectToAction("GuestBooking","Home");
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
       
        [HttpPost]
        public ActionResult UpdateDetails(LoginViewModel viewmodel)
        {
            Person person = new Person();
            PersonLogic personLogic = new PersonLogic();
             personLogic.Modify(viewmodel.Person);
            SetMessage("Update Successfully!", Message.Category.Information);
            return RedirectToAction("Index", "Home");
           
        }
        public ActionResult Standard(int id)
        {
            try
            {
                if (_User != null)
                {
                    List<Room> room = new List<Room>();
                    RoomLogic roomLogic = new RoomLogic();
                    room = roomLogic.GetModelsBy(r => r.Room_Id == id);

                    return RedirectToAction("RoomBook", "Booking", new { Area = "Customers", cid = _User.UserId, rid = id });
                }
                else
                {
                    FormsAuthentication.SignOut();
                    RedirectToAction("Login", "Account", new { Area = "Security" });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");

        }

        public ActionResult Executive(int id)
        {
            try
            {
                if (_User != null)
                {
                    List<Room> room = new List<Room>();
                    RoomLogic roomLogic = new RoomLogic();
                    room = roomLogic.GetModelsBy(r => r.Room_Id == id);

                    return RedirectToAction("RoomBook", "Booking", new { Area = "Customers", cid = _User.UserId, rid = id });
                }
                else
                {
                    FormsAuthentication.SignOut();
                    RedirectToAction("Login", "Account", new { Area = "Security" });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");

        }
        public ActionResult Reservation()
        {
            return View();
        }
        
    }
}