﻿using BarcodeLib;
using HMS.Areas.Admin.Models;
using HMS.Areas.Customers.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Customers.Controllers
{
    [Authorize]
    public class BookingController : BaseController
    {
        // GET: Customers/Booking
        public ActionResult Roomtype(int id)
        {
           List<Room> room = new List<Room>();
            RoomLogic roomLogic = new RoomLogic();
            room = roomLogic.GetModelsBy(r=>r.Room_Status_Id == id);
            return View(room);
        }
        public ActionResult Basic()
        {
            try
            {
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelsBy(r => r.Room_Type_Id == 4 && r.Room_Status_Id == 2);
                if (room.Count == 0)
                {
                    SetMessage("No Room Available at the moment", Message.Category.Information);
                    return RedirectToAction("Index", "Home");
                }
                return View(room);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        public ActionResult Invoice()
        {
            return View();
        }
        public ActionResult Executive()
        {
            try
            {
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelsBy(r => r.Room_Type_Id == 2 && r.Room_Status_Id == 2);
                if (room.Count == 0)
                {
                    SetMessage("No Room Available at the moment", Message.Category.Information);
                    return RedirectToAction("Index", "Home");
                }
                return View(room);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        public ActionResult Standard()
        {
            try
            {
                List<Room> room = new List<Room>();
                Room roomm = new Room();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelsBy(r => r.Room_Type_Id == 3 && r.Room_Status_Id == 2);
                if (room.Count == 0)
                {
                    SetMessage("No Room Available at the moment", Message.Category.Information);
                    return RedirectToAction("Index","Home");
                }
                return View(room);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult RoomBook(int cid, int rid)
        {
            try
            {
                RoomLogic roomLogic = new RoomLogic();
               
                Room room = new Room();
                User user = new User();
                UserLogic userLogic = new UserLogic();
                CustomerViewModel viewmodel = new CustomerViewModel();
                ViewBag.PaymentType = Utility.PopulatePaymentTypeSelectList();
                ViewBag.Service = Utility.PopulateServiceSelectList();
                room = roomLogic.GetModelBy(r => r.Room_Id == rid);
                user = userLogic.GetModelBy(c => c.User_Id == cid);
                viewmodel.Room = room;
                viewmodel.User = user;
                viewmodel.RoomBooking = new RoomBookings { CheckInDate = DateTime.Now.Date };
                return View(viewmodel);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult RoomBook(CustomerViewModel viewmodel)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                Payment payment = new Payment();
                RoomBookings roomBooking = new RoomBookings();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                //if ((viewmodel.RoomBooking.CheckInDate.Day < DateTime.Now.Day))
                //{
                //    SetMessage("Invalid Check In Date! Date entered has to be today and above", Message.Category.Error);
                //    ViewBag.PaymentType = Utility.PopulatePaymentTypeSelectList();
                //    ViewBag.Service = Utility.PopulateServiceSelectList();
                //    return View();
                //}
                if (viewmodel.RoomBooking.CheckInDate == viewmodel.RoomBooking.CheckOutDate)
                {
                    SetMessage("Check In and Check Out Date cannot be the same! ", Message.Category.Error);
                    ViewBag.PaymentType = Utility.PopulatePaymentTypeSelectList();
                    ViewBag.Service = Utility.PopulateServiceSelectList();
                    return View();
                }
                Payment newPayment = new Payment();
                room = roomLogic.GetModelBy(r=>r.Room_Id == viewmodel.Room.RoomId);
                viewmodel.Room = room;
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    payment = CreatingPayment1(viewmodel);
                    newPayment = paymentLogic.GetModelBy(p => p.Payment_Id == payment.PaymentId);
                    DateTime startTime = DateTime.Now;
                    viewmodel.RoomBooking.ArrivalTime = startTime;
                    
                    roomBooking = CreateBooking(viewmodel);
                    viewmodel.Payment.PaymentType = newPayment.PaymentType;
                    viewmodel.Payment.Person = newPayment.Person;
                    viewmodel.RoomBooking = roomBooking;
                    //roomBookingPayment = CreateRoomBookingPayment(viewmodel);
                    viewmodel.Payment = new Payment { PaymentId = newPayment.PaymentId, Pin = newPayment.Pin };
                    transaction.Complete();
                }
                //person = personLogic.GetModelBy();
                if (newPayment.PaymentType.PaymentTypeId == 1)
                {
                    return RedirectToAction("PaymentInvoice", "Booking", new { cid = viewmodel.User.Person.PersonId, pid = viewmodel.Payment.PaymentId });
                }
                else
                {
                    return RedirectToAction("OnlinePayment", "Booking");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
           
            return View();
        }
        public ActionResult OnlinePayment()
        {
            return View();
        }
        public ActionResult PaymentInvoice(int cid, int pid)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                List<Payment> payment = new List<Payment>();
                User user = new User();
                UserLogic userLogic = new UserLogic();
                CustomerViewModel viewmodel = new CustomerViewModel();
                RoomBookingLogic bookingLogic = new RoomBookingLogic();
               RoomBookings booking = new RoomBookings();
                payment = paymentLogic.GetModelsBy(p => p.Person_Id == cid && p.Payment_Id == pid);
                user = userLogic.GetModelBy(c => c.Person_Id == cid);
                //booking = bookingLogic.GetModelBy(c => c.Person_Id== cid && c.Payment_Id == pid && c.Paid == false);
                //viewmodel.Payment = booking.Payment;
                //viewmodel.User = user;
                //viewmodel.RoomBooking = booking;

                return View(viewmodel);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult ListOfBooking()
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
                AdminViewModel viewmodel = new AdminViewModel();
                List<RoomBookings> roomBooking = new List<RoomBookings>();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                roomBooking = roomBookingLogic.GetAll();
                viewmodel.RoomBooking = roomBooking;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult DailyBookins()
        {
            try
            {
                List<RoomBookings> roomBooking = new List<RoomBookings>();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                var date = DateTime.Now;
                roomBooking = roomBookingLogic.GetModelsBy(b=>b.BookingDate ==date);
                return View(roomBooking);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        public ActionResult CheckIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CheckIn(AdminViewModel viewmodel)
        {
            try
            {
                AdminViewModel viewmodels = new AdminViewModel();
                RoomBookings roomBooking = new RoomBookings();
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                Paystack paystack = new Paystack();
                PaystackLogic paystackLogic = new PaystackLogic();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                payment = paymentLogic.GetModelBy(p=>p.Pin == viewmodel.Payment.Pin);
                roomBooking = roomBookingLogic.GetModelBy(r => r.Room_Booking_Id == payment.RoomBookings.RoomBookingId);
                if (roomBooking.CheckInStatus.RoomStatusId != 5 && roomBooking.CheckInStatus.RoomStatusId != 8)
                {
                    SetMessage("Checked In Already!",Message.Category.Error);
                    return View();
                }
                if (payment == null)
                {
                    SetMessage("No Bookings Found! Ensure you entered the correct pin", Message.Category.Error);
                    return View();
                }
              
                paystack = paystackLogic.GetModelBy(p=>p.Payment_Id == payment.PaymentId);
                roomBookingPayment = roomBookingPaymentLogic.GetModelBy(p => p.Payment_Id == payment.PaymentId);

                if (paystack != null || roomBookingPayment != null)
                {
                    //roomBooking = roomBookingLogic.GetModelBy(r => r.Payment_Id == payment.PaymentId);
                    viewmodels.Payment = payment;
                    TempData["payment"] = viewmodels;
                    return View(viewmodels);
                }
                SetMessage("You have not made your payment!", Message.Category.Error);
                return View();
            }
            catch (Exception ex)
            {
                SetMessage("Error Occureed!", Message.Category.Error);
                return View();
                //throw ex;
            }
            
           // return View();
        }
        public ActionResult CheckOut()
        {
            List<RoomBookings> roomBookings = new List<RoomBookings>();
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            roomBookings = roomBookingLogic.GetModelsBy(r => r.Check_In_Status_Id == 6);
            return View(roomBookings);
        }
        [HttpPost]
        public ActionResult CheckOut(AdminViewModel viewmodel)
        {
            try
            {
               List< RoomBookings> roomBookings = new List<RoomBookings>();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                roomBookings = roomBookingLogic.GetModelsBy(r => r.Check_In_Status_Id == 3);
                return View(roomBookings);

               // AdminViewModel viewmodels = new AdminViewModel();
               // RoomBookings roomBooking = new RoomBookings();
               // Payment payment = new Payment();
               // PaymentLogic paymentLogic = new PaymentLogic();
               //// RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
               // Paystack paystack = new Paystack();
               // PaystackLogic paystackLogic = new PaystackLogic();
               // payment = paymentLogic.GetModelBy(p => p.Pin == viewmodel.Payment.Pin);
               // if (payment == null)
               // {
               //     SetMessage("No Bookings Found! Ensure you entered the correct pin", Message.Category.Error);
               //     return View();
               // }

               // paystack = paystackLogic.GetModelBy(p => p.Payment_Id == payment.PaymentId);
               // if (paystack != null)
               // {
               //     //roomBooking = roomBookingLogic.GetModelBy(r => r.Payment_Id == payment.PaymentId);
               //     viewmodels.Payment = payment;
               //     TempData["payment"] = viewmodels;
               //     return View(viewmodels);
               // }
               // SetMessage("You have not made your payment!", Message.Category.Error);
               // return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult UpdatePayment(CustomerViewModel viewmodel)
        {
            try
            {
                AdminViewModel viewmodels = new AdminViewModel();
                viewmodels = (AdminViewModel)TempData["payment"];
                TempData["payment1"] = viewmodels;
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                RoomBookings roomBooking = new RoomBookings();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    //roomBookingPayment = roomBookingPaymentLogic.GetModelBy(r=>r.Payment_Id == viewmodel.RoomBooking.Payment.PaymentId);
                    if (roomBookingPayment != null)
                    {
                        SetMessage("Checked In Already!",Message.Category.Error);
                        return RedirectToAction("Index", "Home", new { Area = "Admin" });
                    }
                    roomBookingPayment = CreateRoomBookingPayment(viewmodel);
                    room = roomLogic.GetModelBy(r => r.Room_Type_Id == viewmodel.RoomBooking.RoomType.RoomTypeId);
                    roomBooking = roomBookingLogic.GetModelBy(r=>r.Room_Booking_Id == viewmodel.RoomBooking.RoomBookingId);
                    UpdateRoomBookingCheckIn(roomBooking);
                    UpdateRoomStatus(room);
                    transaction.Complete();
                }
               
                SetMessage("Checked In Successfully!", Message.Category.Information);
                return RedirectToAction("Receipt", "Booking", new { cid = viewmodel.RoomBooking.Person.PersonId });
            }

            catch (Exception ex)
            {
                throw ex;
            }
           // return View();
        }

        private bool UpdateRoomBookingCheckIn(RoomBookings roomBooking)
        {
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            if (roomBookingLogic.UpdateRoomBookingCheckInStatus(roomBooking))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult Receipt(int cid, int pid)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingLogic bookingLogic = new RoomBookingLogic();
                RoomBookings booking = new RoomBookings();
                CustomerViewModel viewModel = new CustomerViewModel();
                Payment payment = new Payment();

                booking = bookingLogic.GetModelBy(b => b.Person_Id == cid);
                payment = paymentLogic.GetModelBy(p => p.Payment_Id == pid);
                viewModel.RoomBooking = booking;
                viewModel.Payment = payment;
                if (viewModel.Payment == null)
                {
                    SetMessage("No receipt found", Message.Category.Error);
                    return RedirectToAction("Index", "Home", new { Area = "Admin" });
                }
                else
                {
                    string id = pid.ToString();
                    Barcode barcode = new Barcode();
                    Image image = barcode.Encode(TYPE.CODE39, id);
                    byte[] imageByteData = imageToByteArray(image);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                     viewModel.Payment.barcodeImageUrl = imageDataURL;
                    return View(viewModel);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }
        public ActionResult Details(int id)
        {
            try
            {
                RoomBookings roomBooking = new RoomBookings();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();

                payment = paymentLogic.GetModelBy(p => p.Room_Booking_Id == id);
                return View(payment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }
        public ActionResult PaymentReport()
        {
            List<RoomBookingPayment> roomBookingPayment = new List<RoomBookingPayment>();
            RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
            roomBookingPayment = roomBookingPaymentLogic.GetAll();
            return View(roomBookingPayment);
        }
       
        private bool UpdateRoomBookingStatus(RoomBookings roomBooking)
        {
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            if (roomBookingLogic.UpdateRoomBookingDepartureTime(roomBooking))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool UpdateRoomBookingCheckOut(RoomBookings roomBooking)
        {
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            if (roomBookingLogic.UpdateRoomBookingCheckOut(roomBooking))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool UpdateRoomStatus(Room room)
        {
            RoomLogic roomLogic = new RoomLogic();
            if (roomLogic.UpdateRoomStatusToCheckedIn(room))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool UpdateRoomStatusCheckOut(Room room)
        {
            RoomLogic roomLogic = new RoomLogic();
            if (roomLogic.UpdateRoomStatusToCheckedOut(room))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private RoomBookingPayment CreateRoomBookingPayment(CustomerViewModel viewmodel)
        {
            try
            {
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomBookingPaymentLogic roomBookingPaymentlogic = new RoomBookingPaymentLogic();
                roomBookingPayment.RoomBooking = new RoomBookings { RoomBookingId = viewmodel.RoomBooking.RoomBookingId };
               // roomBookingPayment.Payment = new Payment { PaymentId = viewmodel.RoomBooking.Payment.PaymentId };
                roomBookingPayment = roomBookingPaymentlogic.Create(roomBookingPayment);
                return roomBookingPayment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private RoomBookings CreateBooking(CustomerViewModel viewmodel)
        {
            try
            {
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBooking = new RoomBookings();
                roomBooking.CheckInDate = viewmodel.RoomBooking.CheckInDate;
                roomBooking.Person = new Person { PersonId = viewmodel.RoomBooking.Person.PersonId };
                roomBooking.RoomType = new RoomType { RoomTypeId = viewmodel.RoomBooking.RoomType.RoomTypeId };
                //roomBooking.Payment = new Payment { PaymentId = viewmodel.RoomBooking.Payment.PaymentId };
                roomBooking.CheckOutDate = viewmodel.RoomBooking.CheckOutDate;
                roomBooking.ArrivalTime = DateTime.Now;
                roomBooking.Paid = false;
                roomBooking.CheckInStatus = new RoomStatus { RoomStatusId = 5};
                roomBooking.BookingDate = DateTime.Now;
                roomBooking = roomBookingLogic.Create(roomBooking);
                //if (roomBooking != null)
                //{

                //}
               
                return roomBooking;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        private RoomBookings CreateBookings(AdminViewModel viewmodel)
        {
            try
            {
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBooking = new RoomBookings();
                roomBooking.CheckInDate = viewmodel.RoomBookings.CheckInDate;
                roomBooking.Person = new Person { PersonId = viewmodel.RoomBookings.Person.PersonId };
                roomBooking.RoomType = new RoomType { RoomTypeId = viewmodel.RoomBookings.RoomType.RoomTypeId };
                //roomBooking.Payment = new Payment { PaymentId = viewmodel.RoomBooking.Payment.PaymentId };
                roomBooking.CheckOutDate = viewmodel.RoomBookings.CheckOutDate;
                roomBooking.ArrivalTime = DateTime.Now;
                roomBooking.Paid = false;
                roomBooking.CheckInStatus = new RoomStatus { RoomStatusId = 8 };
                roomBooking.BookingDate = DateTime.Now;
                roomBooking.NumOfNight = viewmodel.RoomBookings.NumOfNight;
                roomBooking.NumOfRooms = viewmodel.RoomBookings.NumOfRooms;
                roomBooking = roomBookingLogic.Create(roomBooking);
                //if (roomBooking != null)
                //{

                //}

                return roomBooking;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult BookRoom()
        {

            return RedirectToAction("Index","Home");
        }
        //[HttpPost]
        //public ActionResult BookRoom()
        //{

        //    return View();
        //}
        private RoomBookings CreateBooking1(CustomerViewModel viewmodel)
        {
            try
            {
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBooking = new RoomBookings();
                roomBooking.CheckInDate = viewmodel.RoomBooking.CheckInDate;
                roomBooking.Person = new Person { PersonId = viewmodel.RoomBooking.Person.PersonId };
                roomBooking.RoomType = new RoomType { RoomTypeId = viewmodel.RoomBooking.RoomType.RoomTypeId };
               // roomBooking.Payment = new Payment { PaymentId = viewmodel.RoomBooking.Payment.PaymentId };
                roomBooking.CheckOutDate = viewmodel.RoomBooking.CheckOutDate;
                roomBooking.ArrivalTime = DateTime.Now;
                roomBooking.Paid = false;
                roomBooking.CheckedIn = false;
                roomBooking.CheckedOut = false;
                roomBooking = roomBookingLogic.Create(roomBooking);

                return roomBooking;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Payment CreatingPayment(CustomerViewModel viewmodel)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                Service service = new Service();
                ServiceLogic serviceLogic = new ServiceLogic();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                PaymentType paymentType = new PaymentType();
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelBy(r=>r.Room_Id == viewmodel.RoomBooking.RoomType.RoomTypeId);
                int numOfDays;
                decimal? amount;
                  numOfDays = viewmodel.RoomBooking.CheckOutDate.Day - viewmodel.RoomBooking.CheckInDate.Day;
                amount = room.RoomType.RoomCharge * numOfDays;
                roomType = roomTypeLogic.GetModelBy(r => r.Room_Type_Id == viewmodel.RoomBooking.RoomType.RoomTypeId);
                service = serviceLogic.GetModelBy(s=>s.Service_Id == viewmodel.Payment.Service.ServiceId);
                payment.DatePaid = DateTime.Now;
                payment.Description = "Room Reservation Payment made"+" On "+DateTime.Now ;
                payment.Service = service;
                payment.PaymentType = new PaymentType { PaymentTypeId = viewmodel.Payment.PaymentType.PaymentTypeId };
               // payment.Person = new Person { PersonId = viewmodel.RoomBooking.Payment.Person.PersonId };
                payment.Amount = viewmodel.Payment.Amount;
                Payment newPayment = paymentLogic.Create(payment);
                return newPayment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Payment CreatingPayment1(CustomerViewModel viewmodel)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomType roomType = new RoomType();
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                Service service = new Service();
                ServiceLogic serviceLogic = new ServiceLogic();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                PaymentType paymentType = new PaymentType();
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelBy(r => r.Room_Id == viewmodel.Room.RoomId);
                int numOfDays;
                decimal? amount;
                numOfDays = viewmodel.RoomBooking.CheckOutDate.Day - viewmodel.RoomBooking.CheckInDate.Day;
                amount = room.RoomType.RoomCharge * numOfDays;
                roomType = roomTypeLogic.GetModelBy(r => r.Room_Type_Id == viewmodel.Room.RoomType.RoomTypeId);
                //service = serviceLogic.GetModelBy(s => s.Service_Id == viewmodel.Payment.Service.ServiceId);
                payment.DatePaid = DateTime.Now;
                payment.Description = "Room Reservation Payment made" + " On " + DateTime.Now;
                payment.Service = new Service { ServiceId = 3};
                payment.PaymentType = new PaymentType { PaymentTypeId = viewmodel.Payment.PaymentType.PaymentTypeId };
                payment.Person = new Person { PersonId = viewmodel.User.Person.PersonId };
                payment.Amount = amount;
                Payment newPayment = paymentLogic.Create(payment);
                return newPayment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult RoomCheckIn(int id)
        {
            try
            {
                AdminViewModel viewmodel = new AdminViewModel();
                viewmodel = (AdminViewModel)TempData["payment"];
                TempData["payment2"] = viewmodel;
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelsBy(r => r.Room_Type_Id == id && r.Room_Status_Id == 2);
                return View(room);
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }

        public ActionResult RoomCheckOut(int id)
        {
            try
            {
                AdminViewModel viewmodel = new AdminViewModel();
                viewmodel = (AdminViewModel)TempData["payment"];
                TempData["payment2"] = viewmodel;
                List<Room> room = new List<Room>();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelsBy(r => r.Room_Type_Id == id && r.Room_Status_Id == 3);
                return View(room);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult FinalCheckIn(int id)
        {
            try
            {
                Room room = new Room();
                RoomLogic roomLogic = new RoomLogic();
                room = roomLogic.GetModelBy(r => r.Room_Id == id);
                AdminViewModel viewmodel = new AdminViewModel();
                PaymentLogic paymentLogic = new PaymentLogic();
                Payment payment = new Payment();
                viewmodel = (AdminViewModel)TempData["payment2"];
                if (viewmodel == null)
                {
                    return RedirectToAction("CheckIn","Booking",new { Area= "Customers"});
                }
                ViewBag.paymentId = viewmodel.Payment.PaymentId;
                ViewBag.CheckInStatus = Utility.PopulateRoomStatusSelectList();
                ViewBag.bookingId = viewmodel.Payment.RoomBookings.RoomBookingId;
                viewmodel.Room = room;
                viewmodel.Payment = new Payment { PaymentId = viewmodel.Payment.PaymentId };
                payment = paymentLogic.GetModelBy(p=>p.Payment_Id == viewmodel.Payment.PaymentId );
                viewmodel.Payment = payment;
                viewmodel.RoomBookings = new RoomBookings { RoomBookingId = viewmodel.Payment.RoomBookings.RoomBookingId };
                return View(viewmodel);

            }
            catch (Exception ex )
            {
                ViewBag.CheckInStatus = Utility.PopulateCheckInSelectList();
                SetMessage("Error Occured! " + ex, Message.Category.Error);
                return View();
               
            }
                   }


        [HttpPost]
        public ActionResult FinalCheckIn(AdminViewModel viewModel)
        {
            Payment payment = new Payment();
            PaymentLogic paymentLogic = new PaymentLogic();
            Room room = new Room();
            RoomLogic roomLogic = new RoomLogic();
            payment = paymentLogic.GetModelBy(p => p.Payment_Id == viewModel.Payment.PaymentId);
            room = roomLogic.GetModelBy(r => r.Room_Id == viewModel.Room.RoomId);
            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                payment.RoomBookings.Room = new Room { RoomId = room.RoomId} ;
                UpdateRoomBookingStatus(payment.RoomBookings);
                UpdateRoomStatus(room);
                transaction.Complete();
            }
            SetMessage("Checked In Successfully!", Message.Category.Information);
            return RedirectToAction("Receipt", "Booking", new { cid = payment.Person.PersonId ,pid= payment.PaymentId});
           
        }

        public ActionResult FinalCheckOut(int id)
        {
            try
            {
                //Room room = new Room();
                //RoomLogic roomLogic = new RoomLogic();
                //room = roomLogic.GetModelBy(r => r.Room_Id == id);
                //AdminViewModel viewmodel = new AdminViewModel();
                //viewmodel = (AdminViewModel)TempData["payment2"];
                //if (viewmodel == null)
                //{
                //    return RedirectToAction("CheckIn", "Booking", new { Area = "Customers" });
                //}
                //ViewBag.paymentId = viewmodel.Payment.PaymentId;
                ViewBag.CheckInStatus = Utility.PopulateRoomStatusSelectList();
                //ViewBag.bookingId = viewmodel.Payment.RoomBookings.RoomBookingId;
                //viewmodel.Room = room;
                //viewmodel.Payment = new Payment { PaymentId = viewmodel.Payment.PaymentId };
                //viewmodel.RoomBookings = new RoomBookings { RoomBookingId = viewmodel.RoomBookings.RoomBookingId };
                //return View(viewmodel);
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings roomBookings = new RoomBookings();
                AdminViewModel viewmodel = new AdminViewModel();
                roomBookings = roomBookingLogic.GetModelBy(r => r.Room_Booking_Id == id);
                viewmodel.RoomBookings = roomBookings;
                return View(viewmodel);

            }
            catch (Exception ex)
            {
                ViewBag.CheckInStatus = Utility.PopulateCheckInSelectList();
                SetMessage("Error Occured! " + ex, Message.Category.Error);
                return View();

            }
        }

        [HttpPost]
        public ActionResult FinalCheckOut(AdminViewModel viewModel)
        {
            Payment payment = new Payment();
            PaymentLogic paymentLogic = new PaymentLogic();
            Room room = new Room();
            RoomLogic roomLogic = new RoomLogic();
            RoomBookings roomBookings = new RoomBookings();
            RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
            //payment = paymentLogic.GetModelBy(p => p.Payment_Id == viewModel.Payment.PaymentId);
            
            roomBookings = roomBookingLogic.GetModelBy(r=>r.Room_Booking_Id == viewModel.RoomBookings.RoomBookingId);
            room = roomLogic.GetModelBy(r => r.Room_Id == roomBookings.Room.RoomId);
            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
            {
                roomBookings.Room = new Room { RoomId = room.RoomId};
                UpdateRoomBookingCheckOut(roomBookings);
                UpdateRoomStatusCheckOut(room);
                transaction.Complete();
            }
            SetMessage("Checked Out Successfully!", Message.Category.Information);
            return RedirectToAction("Index","Home",new { Area="Admin"});

        }

        public ActionResult MakeReservation()
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();

                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                //AdminViewModel viewmodel = new AdminViewModel();
               // person = CreatePerson(viewmodel.Person);
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpPost]
        public ActionResult MakeReservation(AdminViewModel viewmodel)
        {
            try
            {
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                RoomBookings roomBookings = new RoomBookings();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                RoomBookings NewRoomBooking = new RoomBookings();
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    
                   person = CreatePerson(viewmodel);
                    Person NewPerson = new Person();
                    NewPerson = personLogic.GetModelBy(p=>p.Person_Id == person.PersonId);
                    viewmodel.RoomBookings.Person = NewPerson;
                    roomBookings = CreateBookings(viewmodel);
                    
                    NewRoomBooking = roomBookingLogic.GetModelBy(r=>r.Room_Booking_Id == roomBookings.RoomBookingId);

                    transaction.Complete();
                }
                TempData["roomBooking"] = NewRoomBooking;
                return RedirectToAction("FrontDeskPayment", "Booking", new { Area ="Customers"});
            }
            catch (Exception ex)
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
                SetMessage("Error Occured!", Message.Category.Error);
                return View();
                
            }
            
        }

        public ActionResult FrontDeskPayment()
        {
            try
            {
                AdminViewModel viewmodel = new AdminViewModel();
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                RoomBookings roomBookings = new RoomBookings();
                Payment payment = new Payment();
                roomBookings = (RoomBookings)TempData["roomBooking"];
                person = personLogic.GetModelBy(p => p.Person_Id == roomBookings.Person.PersonId);
                viewmodel.Person = person;
                viewmodel.Payment.Amount = roomBookings.RoomType.RoomCharge * roomBookings.NumOfNight * roomBookings.NumOfRooms;
                viewmodel.RoomBookings = roomBookings;
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpPost]
        public ActionResult FrontDeskPayment(AdminViewModel viewmodel)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                RoomBookingPayment roomBookingPayment = new RoomBookingPayment();
                RoomBookingPaymentLogic roomBookingPaymentLogic = new RoomBookingPaymentLogic();
                payment.Service = new Service { ServiceId = 4 };
                payment.Amount = viewmodel.Payment.Amount;
                payment.Description = viewmodel.Payment.Description;
                payment.Person = new Person { PersonId = viewmodel.RoomBookings.Person.PersonId };
                payment.PaymentType = new PaymentType { PaymentTypeId = 1 };
                payment.DatePaid = DateTime.Now;
                payment.Pin = viewmodel.Payment.Pin;
                payment.RoomBookings = new RoomBookings { RoomBookingId = viewmodel.RoomBookings.RoomBookingId };
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    Payment NewPayment = paymentLogic.DeskPayment(payment);
                    roomBookingPayment.Payment = new Payment { PaymentId = NewPayment.PaymentId };
                    roomBookingPayment.RoomBooking = new RoomBookings { RoomBookingId = viewmodel.RoomBookings.RoomBookingId };
                    RoomBookingPayment NewRoomBookingPayment = roomBookingPaymentLogic.Create(roomBookingPayment);
                    transaction.Complete();
                }
                return RedirectToAction("FrontDeskReservations","Booking");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        private Person CreatePerson(AdminViewModel viewmodel)
        {
            try
            {
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();

                person = viewmodel.Person;

                Person newPerson = personLogic.Create(person);
                return newPerson;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult FrontDeskReservations()
        {
            try
            {
                AdminViewModel viewModel = new AdminViewModel();
                List<RoomBookings> roomBookings = new List<RoomBookings>();
                RoomBookingLogic roomBookingLogic = new RoomBookingLogic();
                roomBookings = roomBookingLogic.GetModelsBy(r => r.Check_In_Status_Id == 8);
                viewModel.RoomBooking = roomBookings;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
    }
}