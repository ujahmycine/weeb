﻿using HMS.Business;
using HMS.Model.Model;
using HMS.Models;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace HMS.Areas.Security.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Security/Account
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
            ViewBag.Country = Utility.PopulateCountrySelectList();
            ViewBag.Gender = Utility.PopulateGenderSelectList();
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel viewModel, string returnUrl)
        {

            try
            {
                UserLogic userLogic = new UserLogic();
                User userDEtails = new User();
                userDEtails = userLogic.GetModelBy(u => u.Username == viewModel.UserName && u.Password_Hash == viewModel.Password);
                if (userDEtails.AccountType.AccountTypeId == 1 || userDEtails.AccountType.AccountTypeId == 1003)
                {
                    if (userLogic.ValidateUsers(viewModel.UserName, viewModel.Password))
                    {
                        FormsAuthentication.SetAuthCookie(viewModel.UserName, true);
                        var user = userLogic.GetBy(viewModel.UserName);
                        Session["user"] = user;
                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            return RedirectToAction("Index", "Home", new { Area = "Admin" });
                        }
                        else
                        {
                            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                            ViewBag.Country = Utility.PopulateCountrySelectList();
                            ViewBag.Gender = Utility.PopulateGenderSelectList();

                            return RedirectToLocal(returnUrl);
                        }
                    }
                }
                else
                {
                    if (userLogic.ValidateUsers(viewModel.UserName, viewModel.Password))
                    {
                        FormsAuthentication.SetAuthCookie(viewModel.UserName, true);
                        var user = userLogic.GetBy(viewModel.UserName);
                        Session["user"] = user;
                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            return RedirectToAction("Index", "Home", new { Area = "Customers" });
                        }
                        else
                        {
                            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                            ViewBag.Country = Utility.PopulateCountrySelectList();
                            ViewBag.Gender = Utility.PopulateGenderSelectList();
                            return RedirectToLocal(returnUrl);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            SetMessage("Invalid Username or Password!", Message.Category.Error); ViewBag.IdType = Utility.PopulateIdTypeSelectList();
            ViewBag.Country = Utility.PopulateCountrySelectList();
            ViewBag.Gender = Utility.PopulateGenderSelectList();
            return View();

        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.IdType = Utility.PopulateIdTypeSelectList();
            ViewBag.Country = Utility.PopulateCountrySelectList();
            ViewBag.Gender = Utility.PopulateGenderSelectList();
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Home(string ReturnUrl)
        {
            return RedirectToAction("Index","Home");
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(LoginViewModel viewmodel, string returnUrl)
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                PersonLogic personLogic = new PersonLogic();
                UserLogic userLogic = new UserLogic();
                Person person = new Person();
                User user = new User();
                if (personLogic.CheckDuplicateIdType(viewmodel.Email))
                {
                    SetMessage("User Exist Already!", Message.Category.Error);
                }
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    person = CreatePerson(viewmodel);
                    viewmodel.User.Person = person;
                    user = CreateUser(viewmodel);

                    transaction.Complete();
                }
                if (person != null && user != null)
                {
                    Person NewPerson = personLogic.GetModelBy(p => p.Person_Id == person.PersonId);
                    int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    string password = ConfigurationManager.AppSettings["password"];
                    string from = ConfigurationManager.AppSettings["from"];
                    string host = ConfigurationManager.AppSettings["host"];
                    Utility utiz = new Utility();
                    utiz.SendMail(NewPerson.Email, "Hello " + NewPerson.FullName + " Welcome to Lloydant Hotels <br/> We are Please to have your Here! You can make your room reservations and a lot more!!!", "Lloydant Hotel Notification ", from, password, host, port);

                }
                if (userLogic.ValidateUsers(user.UserName, user.PasswordHash))
                {
                    FormsAuthentication.SetAuthCookie(user.UserName, true);
                    var userinfo = userLogic.GetBy(user.UserName);
                    Session["user"] = userinfo;
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Index", "Home", new { Area = "Customers" });
                    }
                    else
                    {
                        return RedirectToLocal(returnUrl);
                    }
                }
                // return RedirectToAction("Index", "Home", new { Area = "Customers" });
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex, Message.Category.Error);
            }

            return View();
        }

        private User CreateUser(LoginViewModel viewmodel)
        {
            try
            {
                User user = new User();
                UserLogic userLogic = new UserLogic();
                user.AccountType = new AccountType { AccountTypeId = 2 };
                user.Active = true;
                user.PasswordHash = viewmodel.User.PasswordHash;
                user.UserName = viewmodel.Email;
                user.LastLogin = DateTime.Now;
                user.Person = new Person { PersonId = viewmodel.User.Person.PersonId };
                User newUser = userLogic.Create(user);
                return newUser;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private Person CreatePerson(LoginViewModel viewmodel)
        {
            try
            {
                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();
                //person.Surname = viewmodel.Surname;
                //person.FirstName = viewmodel.FirstName;
                //person.OtherName = viewmodel.OtherName;
                //person.Address = viewmodel.Address;
                //person.Email = viewmodel.Email;
                //person.MobilePhone = viewmodel.PhoneNumber;
                //person.Country = new Country { CountryId = viewmodel.Country.CountryId};
                //person.IdType = new IdType { IdTypeId = viewmodel.IdType.IdTypeId};
                //person.Gender = new Gender { GenderId = viewmodel.Gender.GenderId};
                //person.IsCorperate = viewmodel.Person.IsCorperate;
                // person.IdNumber = viewmodel.IdNumber;
                person = viewmodel.Person;
                person.Country = new Country { CountryId = 1 };
                person.IdType = new IdType { IdTypeId = 1 };
                person.Gender = new Gender { GenderId = 3 };
                person.Email = viewmodel.Email;
                Person newPerson = personLogic.Create(person);
                return newPerson;

            }
            catch (Exception)
            {
                throw;
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", new { Area = "Security" });
        }
    }
}