﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class ServiceController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                ServiceLogic serviceLogic = new ServiceLogic();
                IEnumerable<Service> list;
                list = serviceLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddService(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    ServiceLogic serviceLogic = new ServiceLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Service = serviceLogic.GetModelBy(f => f.Service_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddService(AdminViewModel viewmodel)
        {
            try
            {
                ServiceLogic serviceLogic = new ServiceLogic();
                if (viewmodel.Service.ServiceId == 0)
                {

                    if (serviceLogic.CheckDuplicateIdType(viewmodel.Service.Name))
                    {
                        SetMessage("Account Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        serviceLogic.Create(viewmodel.Service);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "Service", new { Area = "Admin" });
                    }
                }
                else
                {
                    serviceLogic.Modify(viewmodel.Service);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Service", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                AccountTypeLogic AccountTypeLogic = new AccountTypeLogic();
                // Fee fee = new Fee();
                bool deleted = AccountTypeLogic.Delete(f => f.Account_Type_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Service");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "Service");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}