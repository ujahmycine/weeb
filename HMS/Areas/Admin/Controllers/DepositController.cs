﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class DepositController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                DepositLogic depositLogic = new DepositLogic();
                IEnumerable<Deposit> list;
                list = depositLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddDeposit(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    DepositLogic depositLogic = new DepositLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Deposit = depositLogic.GetModelBy(f => f.Deposit_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddDeposit(AdminViewModel viewmodel)
        {
            try
            {
                DepositLogic depositLogic = new DepositLogic();
                if (viewmodel.Deposit.DepositId == 0)
                {
                    // viewModel.user.LastLoginDate = DateTime.Now;
                    depositLogic.Create(viewmodel.Deposit);
                    SetMessage("Added Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Deposit", new { Area = "Admin" });
                    
                }
                else
                {
                    depositLogic.Modify(viewmodel.RoomType);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Deposit", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                DepositLogic depositLogic = new DepositLogic();
                bool deleted = depositLogic.Delete(f => f.Deposit_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Deposit");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "Deposit");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}