﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class RoomStatusController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                RoomStatusLogic roomStatusLogic = new RoomStatusLogic();
                IEnumerable<RoomStatus> list;
                list = roomStatusLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddRoomStatus(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    RoomStatusLogic roomStatusLogic = new RoomStatusLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.RoomStatus = roomStatusLogic.GetModelBy(f => f.Room_Status_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddRoomStatus(AdminViewModel viewmodel)
        {
            try
            {
                RoomStatusLogic roomStatusLogic = new RoomStatusLogic();
                if (viewmodel.RoomStatus.RoomStatusId == 0)
                {

                    if (roomStatusLogic.CheckDuplicateIdType(viewmodel.RoomStatus.Name))
                    {
                        SetMessage("Payment Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        roomStatusLogic.Create(viewmodel.RoomStatus);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "RoomStatus", new { Area = "Admin" });
                    }
                }
                else
                {
                    roomStatusLogic.Modify(viewmodel.RoomStatus);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "RoomStatus", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {

                RoomStatusLogic roomStatusLogic = new RoomStatusLogic();
                // Fee fee = new Fee();
                bool deleted = roomStatusLogic.Delete(f => f.Room_Status_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "RoomStatus");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "RoomStatus");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}