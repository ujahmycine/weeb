﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class CountryController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                CountryLogic countryLogic = new CountryLogic();
                IEnumerable<Country> list;
                list = countryLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddCountry(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    CountryLogic countryLogic = new CountryLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Country = countryLogic.GetModelBy(f => f.Country_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddCountry(AdminViewModel viewmodel)
        {
            try

              {
                CountryLogic countryLogic = new CountryLogic();
                if (viewmodel.Country.CountryId == 0)
                {

                    if (countryLogic.CheckDuplicateFees(viewmodel.Country.Name))
                    {
                        SetMessage("Country already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        countryLogic.Create(viewmodel.Country);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "Country", new { Area = "Admin" });
                    }
                }
                else
                {
                    countryLogic.Modify(viewmodel.Country);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Country", new { Area = "Admin" });
                }

            }          catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                CountryLogic countryLogic = new CountryLogic();
                // Fee fee = new Fee();
                bool deleted = countryLogic.Delete(f => f.Country_Id == id);
                if (deleted)
                {
                    TempData["SucessMessage"] = "Deleted Sucessfully";
                    return RedirectToAction("Index", "Country");
                }
                else
                {
                    TempData["SucessMessage"] = "Error Occure";
                    return RedirectToAction("Index", "Country");
                }

            }
            catch (Exception) { }
            return View();
        }
    }
}