﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class RoomController : BaseController
    {
        public ActionResult Index()
        {
            AdminViewModel adminViewModel = new AdminViewModel();
            try
            {
                RoomLogic roomLogic = new RoomLogic();
                adminViewModel.Rooms = roomLogic.GetAll();
                
            }
            catch (Exception)
            {
                throw;
            }
            return View(adminViewModel);
        }
        public ActionResult AddRoom(int id = 0)
        {
            try
            {
                ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
                ViewBag.RoomStatus = Utility.PopulateRoomStatusSelectList();
                
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    RoomLogic roomLogic = new RoomLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Room = roomLogic.GetModelBy(f => f.Room_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddRoom(AdminViewModel viewmodel)
        {
            try
            {
                RoomLogic roomLogic = new RoomLogic();
                if (viewmodel.Room.RoomId == 0)
                {

                    if (roomLogic.CheckDuplicateIdType(viewmodel.Room.Name))
                    {
                        ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
                        ViewBag.RoomStatus = Utility.PopulateRoomStatusSelectList();
                        SetMessage("Room Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        roomLogic.Create(viewmodel.Room);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "Room", new { Area = "Admin" });
                    }
                }
                else
                {
                    roomLogic.Modify(viewmodel.Room);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Room", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            ViewBag.RoomType = Utility.PopulateRoomTypeSelectList();
            ViewBag.RoomStatus = Utility.PopulateRoomStatusSelectList();
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                RoomLogic roomLogic = new RoomLogic();
                // Fee fee = new Fee();
                bool deleted = roomLogic.Delete(f => f.Room_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Room");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "Room");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}