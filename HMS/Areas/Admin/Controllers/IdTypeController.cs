﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class IdTypeController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                IdTypeLogic idTypeLogic = new IdTypeLogic();
                IEnumerable<IdType> list;
                list = idTypeLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddIdType(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    IdTypeLogic idTypeLogic = new IdTypeLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.IdType = idTypeLogic.GetModelBy(f => f.Id_Type_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddIdType(AdminViewModel viewmodel)
        {
            try
            {
                IdTypeLogic idTypeLogic = new IdTypeLogic();
                if (viewmodel.IdType.IdTypeId == 0)
                {

                    if (idTypeLogic.CheckDuplicateIdType(viewmodel.IdType.Name))
                    {
                        SetMessage("ID Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        idTypeLogic.Create(viewmodel.IdType);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "IdType", new { Area = "Admin" });
                    }
                }
                else
                {
                    idTypeLogic.Modify(viewmodel.IdType);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "IdType", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                IdTypeLogic idTypeLogic = new IdTypeLogic();
                // Fee fee = new Fee();
                bool deleted = idTypeLogic.Delete(f => f.Id_Type_Id == id);
                if (deleted)
                {
                     SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "IdType");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "IdType");
                   
                }

            }
            catch (Exception) { }
            return View();
        }
    }
}