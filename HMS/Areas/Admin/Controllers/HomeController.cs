﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HMS.Areas.Admin.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private User _User;
        private UserLogic userLogic;
        public HomeController()
        {
            try
            {
                if (System.Web.HttpContext.Current.Session["user"] != null)
                {
                    userLogic = new UserLogic();
                    _User = System.Web.HttpContext.Current.Session["user"] as User;
                    _User = userLogic.GetBy(_User.UserId);
                }
                else
                {
                    FormsAuthentication.SignOut();
                    System.Web.HttpContext.Current.Response.Redirect("/Security/Account/Login");

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        // GET: Admin/Home
        public ActionResult Index()
        {
            AdminViewModel viewmodel = new AdminViewModel();
            List<Room> rooms = new List<Room>();
            List<Payment> payments = new List<Payment>();
            List<Person> persons = new List<Person>();
            List<RoomType> roomTypes = new List<RoomType>();
            RoomLogic roomLogic = new RoomLogic();
            PaymentLogic paymentLogic = new PaymentLogic();
            PersonLogic personLogic = new PersonLogic();
            RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
            rooms = roomLogic.GetAll();
            payments = paymentLogic.GetAll();
            persons = personLogic.GetAll();
            roomTypes = roomTypeLogic.GetAll();
            viewmodel.Rooms = rooms;
            viewmodel.Payments = payments;
            viewmodel.Persons = persons;
            viewmodel.RoomTypes = roomTypes;
            return View(viewmodel);
        }
        public ActionResult POS()
        {
            ViewBag.Service = Utility.PopulateServiceSelectList();
            return View();
        }
        [HttpPost]
        public ActionResult POS(AdminViewModel viewmodel)
        {
            Service service = new Service();
            ServiceLogic serviceLogic = new ServiceLogic();

            return View();
        }

        public ActionResult ListOfBooking(int id)
        {
            try
            {
                List<RoomBookings> booking = new List<RoomBookings>();
                RoomBookingLogic bookingLogic = new RoomBookingLogic();
                booking = bookingLogic.GetModelsBy(b => b.Person_Id == id);
                return View(booking);
            }
            catch (Exception)
            {
                throw;
            }

        }
        //public ActionResult ManageUsers()
        //{
        //    ViewBag.AccountType = Utility
        //    return View();
        //}
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
         [HttpPost]
        public ActionResult LogOff(string returnUrl)
        {
            FormsAuthentication.SignOut();
            return RedirectToLocal(returnUrl);
        }
    }
}