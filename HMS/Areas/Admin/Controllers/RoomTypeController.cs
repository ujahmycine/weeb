﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class RoomTypeController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                IEnumerable<RoomType> list;
                list = roomTypeLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddRoomType(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.RoomType = roomTypeLogic.GetModelBy(f => f.Room_Type_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddRoomType(AdminViewModel viewmodel)
        {
            try
            {
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                if (viewmodel.RoomType.RoomTypeId == 0)
                {

                    if (roomTypeLogic.CheckDuplicateIdType(viewmodel.RoomType.Name))
                    {
                        SetMessage("Room Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        roomTypeLogic.Create(viewmodel.RoomType);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "RoomType", new { Area = "Admin" });
                    }
                }
                else
                {
                    roomTypeLogic.Modify(viewmodel.RoomType);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "RoomType", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                // Fee fee = new Fee();
                bool deleted = roomTypeLogic.Delete(f => f.Room_Type_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "RoomType");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "RoomType");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}