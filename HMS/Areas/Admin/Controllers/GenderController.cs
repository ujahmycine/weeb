﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class GenderController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                GenderLogic genderLogic = new GenderLogic();
                IEnumerable<Gender> list;
                list = genderLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddGender(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    GenderLogic genderLogic = new GenderLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Gender = genderLogic.GetModelBy(f => f.Gender_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddGender(AdminViewModel viewmodel)
        {
            try
            {
                GenderLogic genderLogic = new GenderLogic();
                if (viewmodel.Gender.GenderId == 0)
                {

                    if (genderLogic.CheckDuplicateFees(viewmodel.Gender.Name))
                    {
                        SetMessage("Gender already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        genderLogic.Create(viewmodel.Gender);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "Gender", new { Area = "Admin" });
                    }
                }
                else
                {
                    genderLogic.Modify(viewmodel.Gender);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Gender", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}
        
        public ActionResult Delete(int id)
        {
            try
            {
                GenderLogic genderLogic = new GenderLogic();
                // Fee fee = new Fee();
                bool deleted = genderLogic.Delete(f => f.Gender_Id == id);
                if (deleted)
                {
                    TempData["SucessMessage"] = "Deleted Sucessfully";
                    return RedirectToAction("Index", "Gender");
                }
                else
                {
                    TempData["SucessMessage"] = "Error Occure";
                    return RedirectToAction("Index", "Gender");
                }

            }
            catch (Exception) { }
            return View();
        }

    }
}