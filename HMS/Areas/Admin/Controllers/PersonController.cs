﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using HMSPresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class PersonController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                PersonLogic PersonLogic = new PersonLogic();
                IEnumerable<Person> list;
                list = PersonLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddPerson(int id = 0)
        {
            try
            {
                ViewBag.IdType = Utility.PopulateIdTypeSelectList();
                ViewBag.Country = Utility.PopulateCountrySelectList();
                ViewBag.Gender = Utility.PopulateGenderSelectList();
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    PersonLogic personLogic = new PersonLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.Person = personLogic.GetModelBy(f => f.Person_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddPerson(AdminViewModel viewmodel)
        {
            try
            {
                PersonLogic personLogic = new PersonLogic();
                if (viewmodel.Person.PersonId == 0)
                {

                    if (personLogic.CheckDuplicateIdType(viewmodel.Person.Email))
                    {
                        SetMessage("Room Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        personLogic.Create(viewmodel.Person);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "Person", new { Area = "Admin" });
                    }
                }
                else
                {
                    personLogic.Modify(viewmodel.Person);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Person", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                PersonLogic personLogic = new PersonLogic();
                // Fee fee = new Fee();
                bool deleted = personLogic.Delete(f => f.Person_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "Person");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "Person");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}