﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class PaymentTypeController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                IEnumerable<PaymentType> list;
                list = paymentTypeLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddPaymentType(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.PaymentType = paymentTypeLogic.GetModelBy(f => f.Payment_Type_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddPaymentType(AdminViewModel viewmodel)
        {
            try
            {
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                if (viewmodel.PaymentType.PaymentTypeId == 0)
                {

                    if (paymentTypeLogic.CheckDuplicateIdType(viewmodel.PaymentType.Name))
                    {
                        SetMessage("Payment Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        paymentTypeLogic.Create(viewmodel.PaymentType);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "PaymentType", new { Area = "Admin" });
                    }
                }
                else
                {
                    paymentTypeLogic.Modify(viewmodel.PaymentType);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "PaymentType", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                // Fee fee = new Fee();
                bool deleted = paymentTypeLogic.Delete(f => f.Payment_Type_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "PaymentType");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "PaymentType");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}