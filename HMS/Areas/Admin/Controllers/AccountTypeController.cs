﻿using HMS.Areas.Admin.Models;
using HMS.Business;
using HMS.Model.Model;
using HMSPresentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS.Areas.Admin.Controllers
{
    public class AccountTypeController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                AccountTypeLogic AccountTypeLogic = new AccountTypeLogic();
                IEnumerable<AccountType> list;
                list = AccountTypeLogic.GetAll();
                return View(list);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult AddAccountType(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return View(new AdminViewModel());
                }
                else
                {
                    AccountTypeLogic AccountTypeLogic = new AccountTypeLogic();
                    AdminViewModel viewmodel = new AdminViewModel();
                    viewmodel.AccountType = AccountTypeLogic.GetModelBy(f => f.Account_Type_Id == id);
                    return View(viewmodel);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult AddAccountType(AdminViewModel viewmodel)
        {
            try
            {
                AccountTypeLogic AccountTypeLogic = new AccountTypeLogic();
                if (viewmodel.AccountType.AccountTypeId == 0)
                {

                    if (AccountTypeLogic.CheckDuplicateIdType(viewmodel.AccountType.Name))
                    {
                        SetMessage("Account Type already exist!", Message.Category.Error);
                        return View();
                    }
                    else
                    {

                        // viewModel.user.LastLoginDate = DateTime.Now;
                        AccountTypeLogic.Create(viewmodel.AccountType);
                        SetMessage("Added Successfully!", Message.Category.Information);
                        return RedirectToAction("Index", "AccountType", new { Area = "Admin" });
                    }
                }
                else
                {
                    AccountTypeLogic.Modify(viewmodel.AccountType);
                    SetMessage("Update Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "AccountType", new { Area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        //public ActionResult Delete() 
        //{
        //    return View();
        //}

        public ActionResult Delete(int id)
        {
            try
            {
                AccountTypeLogic AccountTypeLogic = new AccountTypeLogic();
                // Fee fee = new Fee();
                bool deleted = AccountTypeLogic.Delete(f => f.Account_Type_Id == id);
                if (deleted)
                {
                    SetMessage("Deleted Successfully!", Message.Category.Information);
                    return RedirectToAction("Index", "AccountType");
                }
                else
                {
                    SetMessage("Error Occured!", Message.Category.Error);
                    return RedirectToAction("Index", "AccountType");

                }

            }
            catch (Exception) { }
            return View();
        }
    }
}