﻿using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMS.Areas.Admin.Models
{
    public class AdminViewModel
    {

        public AdminViewModel()
        {
            Room = new Room();
            Person = new Person();
            Payment = new Payment();
            RoomType = new RoomType();
            RoomBookings = new RoomBookings();
        }
        public Gender Gender { get; set; }
        public Country Country { get; set; }
        public IdType IdType { get; set; }
        public AccountType AccountType { get; set; }
        public PaymentType PaymentType { get; set; }
        public RoomStatus RoomStatus { get; set; }
        public RoomType RoomType { get; set; }
        public Deposit Deposit { get; set; }
        public Person Person { get; set; }
        public Room Room { get; set; }
        public Service Service { get; set; }
        public RoomBookings RoomBookings { get; set; }
        public int Quantity { get; set; }
        public Paystack Paystack { get; set; }
        public Payment Payment { get; set; }
       // public List<RoomType> RoomTypes { get; set; }
        public List<HMS.Model.Model.RoomType> RoomTypes { get; set; }
        public List<HMS.Model.Model.Room> Rooms { get; set; }
        public List<HMS.Model.Model.RoomBookings> RoomBooking { get; set; }
        public List<HMS.Model.Model.Person> Persons { get; set; }
        public List<HMS.Model.Model.Payment> Payments { get; set; }

        public string PromoCode { get; set; }
    }
}