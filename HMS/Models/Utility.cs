﻿using HMS.Business;
using HMS.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace HMSPresentation.Models
{
    public class Utility
    {
        public const string Select = "-- Select --";
        public static List<SelectListItem> PopulateIdTypeSelectList()
        {
            try
            {
                IdTypeLogic idTypeLogic = new IdTypeLogic();
                List<IdType> idTypes = idTypeLogic.GetAll();
                if (idTypes == null || idTypes.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (idTypes != null && idTypes.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (IdType idType in idTypes)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = idType.IdTypeId.ToString();
                        selectList.Text = idType.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateCountrySelectList()
        {
            try
            {
                CountryLogic countryLogic = new CountryLogic();
                List<Country> countries = countryLogic.GetAll();
                if (countries == null || countries.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (countries != null && countries.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (Country country in countries)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = country.CountryId.ToString();
                        selectList.Text = country.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulatePaymentTypeSelectList()
        {
            try
            {
                PaymentTypeLogic paymentTypeLogic = new PaymentTypeLogic();
                List<PaymentType> paymentTypes = paymentTypeLogic.GetAll();
                if (paymentTypes == null || paymentTypes.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (paymentTypes != null && paymentTypes.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (PaymentType paymentType in paymentTypes)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = paymentType.PaymentTypeId.ToString();
                        selectList.Text = paymentType.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateServiceSelectList()
        {
            try
            {
                ServiceLogic serviceLogic = new ServiceLogic();
                List<Service> services = serviceLogic.GetAll();
                if (services == null || services.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (services != null && services.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (Service service in services)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = service.ServiceId.ToString();
                        selectList.Text = service.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateGenderSelectList()
        {
            try
            {
                GenderLogic genderLogic = new GenderLogic();
                List<Gender> genders = genderLogic.GetAll();
                if (genders == null || genders.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (genders != null && genders.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (Gender gender in genders)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = gender.GenderId.ToString();
                        selectList.Text = gender.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateRoomStatusSelectList()
        {
            try
            {
                RoomStatusLogic roomStatusLogic = new RoomStatusLogic();
                List<RoomStatus> roomStatuss = roomStatusLogic.GetAll();
                if (roomStatuss == null || roomStatuss.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (roomStatuss != null && roomStatuss.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (RoomStatus roomStatus in roomStatuss)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = roomStatus.RoomStatusId.ToString();
                        selectList.Text = roomStatus.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateRoomTypeSelectList()
        {
            try
            {
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                List<RoomType> roomTypes = roomTypeLogic.GetAll();
                if (roomTypes == null || roomTypes.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (roomTypes != null && roomTypes.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (RoomType roomType in roomTypes)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = roomType.RoomTypeId.ToString();
                        selectList.Text = roomType.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateAccountTypeSelectList()
        {
            try
            {
                RoomTypeLogic roomTypeLogic = new RoomTypeLogic();
                List<RoomType> roomTypes = roomTypeLogic.GetAll();
                if (roomTypes == null || roomTypes.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (roomTypes != null && roomTypes.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (RoomType roomType in roomTypes)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = roomType.RoomTypeId.ToString();
                        selectList.Text = roomType.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateCheckInSelectList()
        {
            try
            {
                CheckInStatusLogic roomTypeLogic = new CheckInStatusLogic();
                List<CheckInStatus> roomTypes = roomTypeLogic.GetAll();
                if (roomTypes == null || roomTypes.Count <= 0)
                {
                    return new List<SelectListItem>();
                }
                List<SelectListItem> Optionlist = new List<SelectListItem>();
                if (roomTypes != null && roomTypes.Count > 0)
                {
                    SelectListItem list = new SelectListItem();
                    list.Value = "";
                    list.Text = Select;
                    Optionlist.Add(list);
                    foreach (CheckInStatus roomType in roomTypes)
                    {
                        SelectListItem selectList = new SelectListItem();
                        selectList.Value = roomType.Id.ToString();
                        selectList.Text = roomType.Name;
                        Optionlist.Add(selectList);
                    }
                }
                return Optionlist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static User GetUserInfo(string username)
        {
            try
            {
                UserLogic userLogic = new UserLogic();
                return userLogic.GetBy(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static Customer GetCustomerInfo(string email)
        //{
        //    try 
        //    {
        //        CustomerLogic customerLogic = new CustomerLogic();
        //        return customerLogic.GetBy(email);
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool SendMail(string SendTo, string Text, string Subject, string from, string Password, string Host, int Port)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            message.To.Add(SendTo);
            message.Subject = Subject;
            message.Body = Text;
            message.IsBodyHtml = true;
            message.ReplyToList.Add(new MailAddress(from));


            SmtpClient clients = new SmtpClient();
           
            clients.EnableSsl = true;
            clients.Port = Port;
            clients.UseDefaultCredentials = false;
            clients.DeliveryMethod = SmtpDeliveryMethod.Network;
            clients.Host = Host;//smpt.gmail.com
            clients.Credentials = new NetworkCredential(from, Password);



            try
            {
                clients.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}