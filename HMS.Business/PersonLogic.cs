﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class PersonLogic : BusinessBaseLogic<Person, PERSON>
    {
        public PersonLogic()
        {
            translator = new PersonTranslator();
        }




        public bool CheckDuplicateIdType(string email)
        {
            try
            {
                Expression<Func<PERSON, bool>> selector = r => r.Email == email;
                Person personDetails = GetModelBy(selector);
                if (personDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(Person person)
        {
            try
            {
                PERSON personEntity = GetEntityBy(person);
                if (personEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                personEntity.Person_Id = person.PersonId;
                personEntity.Surname = person.Surname;
                personEntity.Address = person.Address;
                personEntity.FirstName = person.FirstName;
                personEntity.OtherName = person.OtherName;
                personEntity.Phone_Number = person.MobilePhone;
                personEntity.Gender_Id = person.Gender.GenderId;
                personEntity.Country_Id = person.Country.CountryId;
                personEntity.Id_Type_Id = person.IdType.IdTypeId;
                personEntity.Is_Corperate_Or_Group = person.IsCorperate;
                personEntity.Id_Number = person.IdNumber;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private PERSON GetEntityBy(Person person)
        {
            try
            {
                Expression<Func<PERSON, bool>> selector = s => s.Person_Id == person.PersonId;
                PERSON entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
