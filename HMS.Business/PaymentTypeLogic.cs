﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class PaymentTypeLogic : BusinessBaseLogic<PaymentType, PAYMENT_TYPE>
    {
        public PaymentTypeLogic() 
        {
            translator = new PaymentTypeTranslator();
        }

        public bool Modify(PaymentType paymentType)
        {
            try
            {
                PAYMENT_TYPE paymentTypeEntity = GetEntityBy(paymentType);
                if (paymentTypeEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                paymentTypeEntity.Payment_Type_Id = paymentType.PaymentTypeId;
                paymentTypeEntity.Name = paymentType.Name;
                paymentTypeEntity.Description = paymentType.Description;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private PAYMENT_TYPE GetEntityBy(PaymentType paymentType)
        {
            try
            {
                Expression<Func<PAYMENT_TYPE, bool>> selector = s => s.Payment_Type_Id == paymentType.PaymentTypeId;
                PAYMENT_TYPE entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<PAYMENT_TYPE, bool>> selector = r => r.Name == name;
                PaymentType accountTypeDetails = GetModelBy(selector);
                if (accountTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
