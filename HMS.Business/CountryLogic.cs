﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class CountryLogic : BusinessBaseLogic<Country, COUNTRY>
    {
        public CountryLogic() 
        {
            translator = new CountryTranslator();
        }

        public bool CheckDuplicateFees(string name)
        {
            try
            {
                Expression<Func<COUNTRY, bool>> selector = r => r.Name == name;
                Country genderDetails = GetModelBy(selector);
                if (genderDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(Country country)
        {
            try
            {
                COUNTRY countryEntity = GetEntityBy(country);
                if (countryEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                countryEntity.Country_Id = country.CountryId;
                countryEntity.Name = country.Name;
                countryEntity.Active = country.Active;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private COUNTRY GetEntityBy(Country country)
        {
            try
            {
                Expression<Func<COUNTRY, bool>> selector = s => s.Country_Id == country.CountryId;
                COUNTRY entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
