﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class AccountTypeLogic : BusinessBaseLogic<AccountType, ACCOUNT_TYPE>
    {
        public AccountTypeLogic() 
        {
            translator = new AccountTypeTranslator();
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<ACCOUNT_TYPE, bool>> selector = r => r.Name == name;
                AccountType accountTypeDetails = GetModelBy(selector);
                if (accountTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(AccountType accountType)
        {
            try
            {
                ACCOUNT_TYPE accountEntity = GetEntityBy(accountType);
                if (accountEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                accountEntity.Account_Type_Id = accountType.AccountTypeId;
                accountEntity.Name = accountType.Name;
                accountEntity.Active = accountType.Active;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private ACCOUNT_TYPE GetEntityBy(AccountType accountType)
        {
            try
            {
                Expression<Func<ACCOUNT_TYPE, bool>> selector = s => s.Account_Type_Id == accountType.AccountTypeId;
                ACCOUNT_TYPE entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
