﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class RoomTypeLogic : BusinessBaseLogic<RoomType, ROOM_TYPE>
    {
        public RoomTypeLogic() 
        {
            translator = new RoomTypeTranslator();
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<ROOM_TYPE, bool>> selector = r => r.Name == name;
                RoomType accountTypeDetails = GetModelBy(selector);
                if (accountTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(RoomType roomType)
        {
            try
            {
                ROOM_TYPE roomTypeEntity = GetEntityBy(roomType);
                if (roomTypeEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                roomTypeEntity.Room_Type_Id = roomType.RoomTypeId;
                roomTypeEntity.Name = roomType.Name;
                roomTypeEntity.Description = roomType.DEscription;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ROOM_TYPE GetEntityBy(RoomType roomType)
        {
            try
            {
                Expression<Func<ROOM_TYPE, bool>> selector = s => s.Room_Type_Id == roomType.RoomTypeId;
                ROOM_TYPE entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
