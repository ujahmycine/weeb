﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class GenderLogic : BusinessBaseLogic<Gender, GENDER>
    {
        public GenderLogic() 
        {
            translator = new GenderTranslator();
        }

        public bool CheckDuplicateFees(string name)
        {
            try 
            {
                Expression<Func<GENDER, bool>> selector = r => r.Name == name ;
                Gender genderDetails = GetModelBy(selector);
                if (genderDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception)
            {
                throw;
            }
        }

        public bool Modify(Gender gender)
        {
            try
            {
                GENDER genderEntity = GetEntityBy(gender);
                if (genderEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                genderEntity.Gender_Id = gender.GenderId;
                genderEntity.Name = gender.Name;
                genderEntity.Active = gender.Active;
               

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private GENDER GetEntityBy(Gender gender)
        {
            try
            {
                Expression<Func<GENDER, bool>> selector = s => s.Gender_Id == gender.GenderId;
                GENDER entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
