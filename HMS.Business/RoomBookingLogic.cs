﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class RoomBookingLogic: BusinessBaseLogic<RoomBookings,ROOM_BOOKING>
    {
        public RoomBookingLogic() 
        {
            translator = new RoomBookingTranslator();
        }

        public bool UpdateRoomBookingDepartureTime(RoomBookings roomBooking)
        {
            Expression<Func<ROOM_BOOKING, bool>> selector = p => p.Room_Booking_Id == roomBooking.RoomBookingId;
            ROOM_BOOKING Entity = GetEntityBy(selector);
            if (Entity == null && Entity.RoomType_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
            Entity.RoomType_Id = roomBooking.RoomType.RoomTypeId;
            Entity.Person_Id = roomBooking.Person.PersonId;
            Entity.Check_In_Date = DateTime.Now;
            Entity.Check_Out_Date = roomBooking.CheckOutDate;
            Entity.Arrival_Time = roomBooking.ArrivalTime;
            Entity.Departure_Time = roomBooking.DepatureTime;
            Entity.BookingDate = roomBooking.BookingDate;
            Entity.Number_Of_Rooms = roomBooking.NumOfRooms;
            Entity.Num_Of_Nights = roomBooking.NumOfRooms;
            Entity.Check_In_Status_Id = 6;
            Entity.Paid = true;
            Entity.Room_Id = roomBooking.Room.RoomId;
           
            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }

        public bool UpdateRoomBookingCheckOut(RoomBookings roomBooking)
        {
            Expression<Func<ROOM_BOOKING, bool>> selector = p => p.Room_Booking_Id == roomBooking.RoomBookingId;
            ROOM_BOOKING Entity = GetEntityBy(selector);
            if (Entity == null && Entity.RoomType_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
            Entity.RoomType_Id = roomBooking.RoomType.RoomTypeId;
            Entity.Person_Id = roomBooking.Person.PersonId;
            Entity.Check_In_Date = DateTime.Now;
            Entity.Check_Out_Date = roomBooking.CheckOutDate;
            Entity.Arrival_Time = roomBooking.ArrivalTime;
            Entity.Departure_Time = roomBooking.DepatureTime;
            Entity.BookingDate = roomBooking.BookingDate;
            Entity.Number_Of_Rooms = roomBooking.NumOfRooms;
            Entity.Num_Of_Nights = roomBooking.NumOfRooms;
            Entity.Check_In_Status_Id = 7;
            Entity.Paid = true;
            Entity.Room_Id = roomBooking.Room.RoomId;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }

        public bool UpdateRoomBookingCheckInStatus(RoomBookings roomBooking)
        {
            Expression<Func<ROOM_BOOKING, bool>> selector = p => p.Room_Booking_Id == roomBooking.RoomBookingId;
            ROOM_BOOKING Entity = GetEntityBy(selector);
            if (Entity == null && Entity.Room_Booking_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
            Entity.RoomType_Id = roomBooking.RoomType.RoomTypeId;
            Entity.Person_Id = roomBooking.Person.PersonId;
            Entity.Check_In_Date = roomBooking.CheckInDate;
            Entity.Check_Out_Date = roomBooking.CheckOutDate;
            Entity.Arrival_Time = roomBooking.ArrivalTime;
            Entity.Departure_Time = DateTime.Now;
            Entity.Paid = false;
          
            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }
    }
    
}
