﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class PaymentLogic:BusinessBaseLogic<Payment,PAYMENT>
    {
        public PaymentLogic() 
        {
            translator = new PaymentTranslator();
        }
        public override Payment Create(Payment payment)
        {
            try
            {
                Payment newPayment = base.Create(payment);
                if(newPayment == null || newPayment.PaymentId <= 0)
                {
                    throw new Exception("Payment Id Not Set");

                }
                newPayment = SetNextPaymentNumber(newPayment);

                SetInvoiceNumber(newPayment);
                return newPayment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Payment DeskPayment(Payment payment)
        {
            try
            {
                Payment NewPayment = base.Create(payment);
                return NewPayment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SetInvoiceNumber(Payment payment)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Payment_Id == payment.PaymentId;
                PAYMENT entity = base.GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }
                entity.Pin = payment.Pin;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private Payment SetNextPaymentNumber(Payment payment)
        {
            try
            {
                try
                {

                    payment.Pin = "HMS" + DateTime.Now.ToString("yy") + PaddNumber(payment.PaymentId, 10);

                    return payment;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string PaddNumber(long id, int maxCount)
        {
            try
            {
                string idInString = id.ToString();
                string paddNumbers = "";
                if (idInString.Count() < maxCount)
                {
                    int zeroCount = maxCount - id.ToString().Count();
                    StringBuilder builder = new StringBuilder();
                    for (int counter = 0; counter < zeroCount; counter++)
                    {
                        builder.Append("0");
                    }

                    builder.Append(id);
                    paddNumbers = builder.ToString();
                    return paddNumbers;
                }

                return paddNumbers;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdatePayment(Payment payment)
        {
            Expression<Func<PAYMENT, bool>> selector = p => p.Payment_Id == payment.PaymentId;
           PAYMENT Entity = GetEntityBy(selector);
            if (Entity == null && Entity.Payment_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
           // Entity.Payment_Id = payment.PaymentId;
            Entity.Person_Id = payment.Person.PersonId;
            Entity.Service_Id = payment.Service.ServiceId;
            Entity.Amount = payment.Amount;
            Entity.Description = payment.Description;
            Entity.Payment_Type_Id = payment.PaymentType.PaymentTypeId;
            Entity.Payment_Gateway = payment.PaymentGateWay;
            Entity.Date_Booked = payment.DatePaid;
            Entity.Pin = payment.Pin;
            Entity.Room_Booking_Id = payment.RoomBookings.RoomBookingId;
            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }
    }
}
