﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class RoomStatusLogic : BusinessBaseLogic<RoomStatus, ROOM_STATUS>
    {
        public RoomStatusLogic() 
        {
            translator = new RoomStatusTranslator();
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<ROOM_STATUS, bool>> selector = r => r.Name == name;
                RoomStatus accountTypeDetails = GetModelBy(selector);
                if (accountTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(RoomStatus roomStatus)
        {
            try
            {
                ROOM_STATUS roomStatusEntity = GetEntityBy(roomStatus);
                if (roomStatusEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                roomStatusEntity.Room_Status_Id = roomStatus.RoomStatusId;
                roomStatusEntity.Name = roomStatus.Name;
                roomStatusEntity.Active = roomStatus.Active;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ROOM_STATUS GetEntityBy(RoomStatus roomStatus)
        {
            try
            {
                Expression<Func<ROOM_STATUS, bool>> selector = s => s.Room_Status_Id == roomStatus.RoomStatusId;
                ROOM_STATUS entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
