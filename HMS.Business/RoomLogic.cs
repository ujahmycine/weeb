﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
   public class RoomLogic : BusinessBaseLogic<Room,ROOM>
    {
       public RoomLogic()
       {
           translator = new RoomTranslator();
       }



       public bool CheckDuplicateIdType(string name)
       {
           try
           {
               Expression<Func<ROOM, bool>> selector = r => r.Name == name;
               Room accountTypeDetails = GetModelBy(selector);
               if (accountTypeDetails != null)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception)
           {
               throw;
           }
       }

       public bool Modify(Room room)
       {
           try
           {
               ROOM roomEntity = GetEntityBy(room);
               if (roomEntity == null)
               {
                   throw new Exception(NoItemFound);
               }
               roomEntity.Room_Id = room.RoomId;
               roomEntity.Name = room.Name;
               roomEntity.Description = room.Description;
               roomEntity.Active = room.Active;
               roomEntity.Max_Occupant = room.Max_Occupant;
               roomEntity.Max_Adult = room.MaxAdult;
               roomEntity.Room_Type_Id = room.RoomType.RoomTypeId;
               roomEntity.Room_Status_Id = room.RoomStatus.RoomStatusId;


               int modifiedRecordCount = Save();
               if (modifiedRecordCount <= 0)
               {
                   return false;
               }
               else
               {
                   return true;
               }
           }
           catch (Exception)
           {
               throw;
           }
       }

       private ROOM GetEntityBy(Room room)
       {
           try
           {
               Expression<Func<ROOM, bool>> selector = s => s.Room_Id == room.RoomId;
               ROOM entity = GetEntityBy(selector);

               return entity;
           }
           catch (Exception)
           {
               throw;
           }
       }

        public bool UpdateRoomStatusToCheckedIn(Room room)
        {
            Expression<Func<ROOM, bool>> selector = p => p.Room_Id == room.RoomId;
            ROOM Entity = GetEntityBy(selector);
            if (Entity == null && Entity.Room_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
            Entity.Name = room.Name;
            Entity.Active = room.Active;
            Entity.Room_Type_Id = room.RoomType.RoomTypeId;
            Entity.Description = room.Description;
            Entity.Max_Occupant = room.Max_Occupant;
            Entity.Max_Adult = room.MaxAdult;
            Entity.Room_Status_Id = 6;
            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }
        public bool UpdateRoomStatusToCheckedOut(Room room)
        {
            Expression<Func<ROOM, bool>> selector = p => p.Room_Id == room.RoomId;
            ROOM Entity = GetEntityBy(selector);
            if (Entity == null && Entity.Room_Id <= 0)
            {
                throw new Exception(NoItemFound);
            }
            Entity.Name = room.Name;
            Entity.Active = room.Active;
            Entity.Room_Type_Id = room.RoomType.RoomTypeId;
            Entity.Description = room.Description;
            Entity.Max_Occupant = room.Max_Occupant;
            Entity.Max_Adult = room.MaxAdult;
            Entity.Room_Status_Id = 2;
            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return true;
            }
            return true;
        }
    }
}
