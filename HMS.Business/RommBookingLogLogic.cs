﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class RommBookingLogLogic : BusinessBaseLogic<RoomBookingLog, ROOM_BOOKING_LOG>
    {
        public RommBookingLogLogic()
        {
            translator = new RoomBookingLogTranslator();
        }

    }
}
