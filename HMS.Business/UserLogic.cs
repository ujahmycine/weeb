﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class UserLogic:BusinessBaseLogic<User,USER>
    {
        public UserLogic() 
        {
            translator = new UserTranslator();
        }

        public bool ValidateUsers(string Username, string Password)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.Username == Username && p.Password_Hash == Password;
                User UserDetails = GetModelBy(selector);
                if (UserDetails != null && UserDetails.PasswordHash != null)
                {
                    UpdateLastLogin(UserDetails);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public User GetBy(int userId)
        {
            try
            {
                Expression<Func<USER, bool>> selector = u => u.User_Id == userId;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool UpdateLastLogin(User user)
        {
            try
            {

                Expression<Func<USER, bool>> selector = p => p.Username == user.UserName && p.Password_Hash == user.PasswordHash;
                USER userEntity = GetEntityBy(selector);
                if (userEntity == null || userEntity.User_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                userEntity.Username = user.UserName;
                userEntity.Password_Hash = user.PasswordHash;
                userEntity.Person_Id = user.Person.PersonId;
                userEntity.Account_Type_Id = user.AccountType.AccountTypeId;
                //userEntity.Security_Question_Id = user.SecurityQuestion.Id;
                // userEntity.Security_Answer = user.SecurityAnswer;
                userEntity.Last_Login = DateTime.Now;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return false;
        }
        public User GetBy(string email)
        {
            try
            {
                Expression<Func<USER, bool>> selector = u => u.Username == email;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
