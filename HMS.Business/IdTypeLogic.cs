﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class IdTypeLogic : BusinessBaseLogic<IdType, ID_TYPE>
    {
        public IdTypeLogic() 
        {
            translator = new IdTypeTranslator();
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<ID_TYPE, bool>> selector = r => r.Name == name;
                IdType idTypeDetails = GetModelBy(selector);
                if (idTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(IdType idType)
        {
            try
            {
                ID_TYPE idTypeEntity = GetEntityBy(idType);
                if (idTypeEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                idTypeEntity.Id_Type_Id = idType.IdTypeId;
                idTypeEntity.Name = idType.Name;
                idTypeEntity.Active = idType.Active;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ID_TYPE GetEntityBy(IdType idType)
        {
            try
            {
                Expression<Func<ID_TYPE, bool>> selector = s => s.Id_Type_Id == idType.IdTypeId;
                ID_TYPE entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
