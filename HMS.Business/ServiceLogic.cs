﻿using HMS.Model.Entity;
using HMS.Model.Model;
using HMS.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Business
{
    public class ServiceLogic : BusinessBaseLogic<Service, SERVICE>
    {
        public ServiceLogic() 
        {
            translator = new ServiceTranslator();
        }

        public bool CheckDuplicateIdType(string name)
        {
            try
            {
                Expression<Func<SERVICE, bool>> selector = r => r.Name == name;
                Service accountTypeDetails = GetModelBy(selector);
                if (accountTypeDetails != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(Service service)
        {
            try
            {
                SERVICE serviceEntity = GetEntityBy(service);
                if (serviceEntity == null)
                {
                    throw new Exception(NoItemFound);
                }
                serviceEntity.Service_Id = service.ServiceId;
                serviceEntity.Name = service.Name;
                serviceEntity.Active = service.Active;


                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private SERVICE GetEntityBy(Service service)
        {
            try
            {
                Expression<Func<SERVICE, bool>> selector = s => s.Service_Id == service.ServiceId;
                SERVICE entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
